FROM adoptopenjdk/openjdk12:alpine-jre

RUN mkdir /app
WORKDIR /app

COPY target/pesquisas*.jar /app/app.jar


RUN adduser -D whale
RUN chown -R whale /app

USER whale

#trocar para porta do aplicativo
EXPOSE 8080 
ENV JAVA_TOOL_OPTIONS -Dfile.encoding=UTF8 -Duser.country=BR -Duser.language=pt -Duser.timezone=America/Sao_Paulo -Djava.net.preferIPv4Stack=true
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
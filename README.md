# MVC Java + Thymeleaf of the MCT [![Build Status](https://travis-ci.org/analuciabolico/pesquisas.png?branch=master)](https://travis-ci.org/analuciabolico/pesquisas/)


## Running pesquisas locally
```
	git clone https://github.com/analuciabolico/pesquisas.git
	cd pesquisas
	./gradlew bootRun
```

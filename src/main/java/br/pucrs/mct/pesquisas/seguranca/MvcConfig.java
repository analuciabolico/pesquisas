package br.pucrs.mct.pesquisas.seguranca;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
	
	@Override
    public void addViewControllers(final ViewControllerRegistry registro) {
        registro.addViewController("/login").setViewName("login");
    }

}

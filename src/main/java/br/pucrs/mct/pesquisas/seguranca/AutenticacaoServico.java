package br.pucrs.mct.pesquisas.seguranca;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.pucrs.mct.pesquisas.modelos.UserSystem;
import br.pucrs.mct.pesquisas.repositorios.AdminRepositorio;

@Service
public class AutenticacaoServico implements UserDetailsService {

    @Autowired
    AdminRepositorio repositorio;

    @Override
    public UserDetails loadUserByUsername(String nomeusuario) {
        Optional<UserSystem> usuario = repositorio.findByNome(nomeusuario);
        if (usuario.isPresent()) {
            return usuario.get();
        } else {
            throw new UsernameNotFoundException("Dados Inválidos");
        }
    }
}

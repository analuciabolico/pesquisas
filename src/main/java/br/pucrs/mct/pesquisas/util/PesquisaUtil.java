package br.pucrs.mct.pesquisas.util;

public final class PesquisaUtil {
    /**
     * Mensagens de erro para o model
     */
    public static final String ERRO_PESQUISA = "mensagemError";

    /**
     * Mapeamentos para o model
     */
    public static final String PESQUISA_MENSAGEM = "mensagem";

    /**
     * Mensagens de erro
     */
    public static final String ERRO = "Erro na sua solicitação.";
    public static final String ERRO_EXCLUSAO = "Erro na exclusão da pesquisa.";
    public static final String PESQUISA_NAO_ENCONTRADA = "Pesquisa não encontrada.";
    public static final String ERRO_PERFIL_VINCULADO = "Existe uma pesquisa ativa neste perfil.";
    public static final String ERRO_SALVAR_PESQUISA = "Erro ao salvar a pesquisa.";
    public static final String ERRO_ESTADO_PESQUISA = "Erro, estado não pode ser vazio.";

    /**
     * Mensagens de sucesso
     */
    public static final String EXCLUIR_MENSAGEM_SUCESSO = "Pesquisa excluída com sucesso.";
    public static final String PESQUISA_SALVA_SUCESSO = "Pesquisa criada com sucesso!";

    private PesquisaUtil() {

    }
}

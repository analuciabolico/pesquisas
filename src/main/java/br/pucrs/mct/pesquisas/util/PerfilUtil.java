package br.pucrs.mct.pesquisas.util;

public final class PerfilUtil {
    /**
     * Mapeamentos e Views
     */
    public static final String REDIRECIONAR_NOVO_PERFIL = "redirect:/perfis/novo";
    public static final String REDIRECIONAR_EDITAR_PERFIL = "redirect:/perfis/{id}/editar";
    public static final String REDIRECIONAR_PERFIL = "redirect:/perfis";
    public static final String VISUALIZAR_NOVO_PERFIL = "perfis/novoPerfil";
    public static final String VISUALIZAR_LISTAR_PERFIL = "perfis/listarPerfil";
    public static final String VISUALIZAR_EDITAR_PERFIL = "perfis/editarPerfil";
    public static final String EXCLUIR_ID_PERFIL = "/{id}/excluir";
    public static final String EDITAR_ID_PERFIL = "/{id}/editar";
    public static final String MUDAR_ESTADO = "/{id}/mudarEstado";
    public static final String NOVO_PERFIL = "/novo";

    /**
     * Mensagens para o model
     */
    public static final String MENSAGEM = "mensagem";
    public static final String MODELO_PERFIL = "perfis";

    /**
     * Mensagens de erro para o model
     */
    public static final String ERRO_PERFIL_DUPLICADO = "mensagemErrorPerfilDuplicado";
    public static final String ERRO_MENSAGEM_PERFIL = "mensagemError";

    /**
     * Mensagens de Sucesso
     */
    public static final String CRIAR_MENSAGEM_SUCESSO = "Perfil criado com sucesso.";
    public static final String EDITAR_MENSAGEM_SUCESSO = "Perfil editado com sucesso.";
    public static final String ESTADO_MENSAGEM_SUCESSO = "Estado alterado com sucesso.";


    /**
     * Mensagens de Erros
     */
    public static final String ERRO_LISTAR_PERFIL = "Erro ao listar Perfis.";
    public static final String ERRO_MUDAR_ESTADO = "Erro ao mudar estado do Perfil.";
    public static final String ERRO_CRIAR_DUPLICADO = "Erro, Perfil com mesmo nome.";
    public static final String ERRO = "Erro na sua solicitação.";


    private PerfilUtil() {

    }
}

package br.pucrs.mct.pesquisas.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminControlador {

	public AdminControlador() {
	}

	@GetMapping("/")
	public String index() {
		return "redirect:/pesquisas";
	}		
}

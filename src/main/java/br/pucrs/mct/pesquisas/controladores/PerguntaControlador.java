package br.pucrs.mct.pesquisas.controladores;

import javax.transaction.Transactional;
import javax.validation.Valid;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.modelos.Categorias;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Pergunta;
import br.pucrs.mct.pesquisas.servicos.PerguntaServico;
import br.pucrs.mct.pesquisas.servicos.ServicoException;

@Controller
@RequestMapping("/perguntas")
public class PerguntaControlador {

    /**
     * Mapeamentos e Views
     */
    private static final String REDIRECIONAR_NOVA_PERGUNTA = "redirect:/perguntas/nova";
    private static final String REDIRECIONAR_EDITAR_PERGUNTA = "redirect:/perguntas/{id}/editar";
    private static final String REDIRECIONAR_PERGUNTA = "redirect:/perguntas";
    private static final String VISUALIZA_NOVA_PERGUNTA = "perguntas/novaPergunta";
    private static final String VISUALIZA_LISTAR_PERGUNTA = "perguntas/listarPergunta";
    private static final String VISUALIZAR_EDITAR_PERGUNTA = "perguntas/editarPergunta";
    private static final String EXCLUIR_ID_PERGUNTA = "/{id}/excluir";
    private static final String EDITAR_ID_PERGUNTA = "/{id}/editar";
    private static final String MUDAR_ESTADO = "/{id}/mudarEstado";
    private static final String NOVA_PERGUNTA = "/nova";

    /**
     * Mapeamentos para o model
     */
    private static final String MENSAGEM = "mensagem";
    private static final String MODELO_PERGUNTA = "perguntas";
    /**
     * Mensagens de erro para o model
     */

    private static final String ERRO_MENSAGEM_PERGUNTA = "Erro ao listar perguntas.";

    /**
     * Mensagens de sucesso
     */
    private static final String CRIAR_MENSAGEM_SUCESSO = "Pergunta criada com sucesso.";
    private static final String EDITAR_MENSAGEM_SUCESSO = "Pergunta editada com sucesso.";
    private static final String ESTADO_MENSAGEM_SUCESSO = "Estado alterado com sucesso.";

    /**
     * Mensagens de erro
     */
    private static final String ERRO_MUDAR_ESTADO = "Erro ao mudar estado da pergunta.";
    private static final String ERRO_CRIAR_PERGUNTA = "Erro ao criar a pergunta.";
    private static final String ERRO_EDITAR_PERGUNTA = "Erro ao editar a pergunta";
    private static final String ERRO = "Erro na sua solicitação";

    /**
     * Variáveis
     */
    private static final String ENUNCIADO = "enunciado";
    private static final String ESTADO = "estado";
    private static final String CATEGORIA = "categoria";
    private static final String IDIOMA = "idioma";

    private final PerguntaServico perguntaServico;

    public PerguntaControlador(PerguntaServico perguntaServico) {
        this.perguntaServico = perguntaServico;
    }

    @GetMapping
    public ModelAndView mostrarListaPergunta() {
        try {
            ModelAndView model = new ModelAndView(VISUALIZA_LISTAR_PERGUNTA);
            List<Pergunta> perguntas = this.perguntaServico.buscarTodasPerguntas();
            model.addObject(CATEGORIA, Arrays.asList(Categorias.values()));
            model.addObject(MODELO_PERGUNTA, perguntas);
            return model;
        } catch (Exception execao) {
            throw new ControladorException(ERRO_MENSAGEM_PERGUNTA, execao);
        }
    }

    @PostMapping
    public ModelAndView listarPerguntasComFiltro(
            @RequestParam(name = ESTADO, required = false) final Estados estado,
            @RequestParam(name = CATEGORIA, required = false) final Categorias categoria,
            @RequestParam(name = IDIOMA, required = false) final Idiomas idioma,
            @RequestParam(name = ENUNCIADO, required = false) final String enunciado) {
        try {
            final ModelAndView model = new ModelAndView(VISUALIZA_LISTAR_PERGUNTA);
            List<Pergunta> perguntas;
            final String trimEnunciado = enunciado.trim();

            if (estado == null) {
                if (categoria == null) {
                    if (idioma == null) {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarTodasPerguntas();
                        } else {
                            perguntas = this.perguntaServico.buscarPorEnunciado(trimEnunciado);
                        }
                    } else {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorIdioma(idioma);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEnunciadoEIdioma(trimEnunciado, idioma);
                        }
                    }
                } else {
                    if (idioma == null) {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorCategoria(categoria);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEnunciadoECategoria(trimEnunciado, categoria);
                        }
                    } else {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorIdiomaECategoria(idioma, categoria);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEnunciadoEIdiomaECategoria(trimEnunciado, idioma,
                                                                                                 categoria);
                        }
                    }
                }
            } else {
                if (categoria == null) {
                    if (idioma == null) {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorEstado(estado);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEnunciadoEEstado(trimEnunciado, estado);
                        }
                    } else {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorEstadoEIdioma(estado, idioma);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEstadoEIdiomaEEnunciado(estado, idioma,
                                                                                              trimEnunciado);
                        }
                    }
                } else {
                    if (idioma == null) {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorEstadoECategoria(estado, categoria);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEstadoEEnunciadoECategoria(estado, trimEnunciado,
                                                                                                 categoria);
                        }
                    } else {
                        if (trimEnunciado.isBlank()) {
                            perguntas = this.perguntaServico.buscarPorEstadoEIdiomaECategoria(estado, idioma,
                                                                                              categoria);
                        } else {
                            perguntas = this.perguntaServico.buscarPorEstadoEEnunciadoEIdiomaECategoria(estado,
                                                                                                        trimEnunciado,
                                                                                                        idioma,
                                                                                                        categoria);
                        }
                    }
                }
            }

            model.addObject(CATEGORIA, Arrays.asList(Categorias.values()));
            model.addObject(MODELO_PERGUNTA, perguntas);
            return model;
        } catch (final Exception excecao) {
            throw new ControladorException(ERRO_MENSAGEM_PERGUNTA, excecao);
        }
    }

    @PostMapping(NOVA_PERGUNTA)
    @Transactional
    public ModelAndView salvarPergunta(@Valid final Pergunta pergunta,
                                       final BindingResult resultado,
                                       final RedirectAttributes atribRedirecionamento) {
        try {
            pergunta.setEstado(Estados.ATIVADA);
            ModelAndView modelo = new ModelAndView(REDIRECIONAR_NOVA_PERGUNTA);
            if (!perguntaServico.validarPergunta(resultado)) {
                return novaPergunta(pergunta);
            } else {
                perguntaServico.salvar(pergunta);
                atribRedirecionamento.addFlashAttribute(MENSAGEM, CRIAR_MENSAGEM_SUCESSO);
                return modelo;
            }
        } catch (Exception excecao) {
            ModelAndView erro = new ModelAndView(REDIRECIONAR_NOVA_PERGUNTA);
            atribRedirecionamento.addFlashAttribute(MENSAGEM, ERRO_CRIAR_PERGUNTA);
            return erro;
        }

    }

    @GetMapping(NOVA_PERGUNTA)
    public ModelAndView novaPergunta(final Pergunta pergunta) {
        try {
            ModelAndView modelo = new ModelAndView(VISUALIZA_NOVA_PERGUNTA);
            modelo.addObject(IDIOMA, Arrays.asList(Idiomas.values()));
            modelo.addObject(CATEGORIA, Arrays.asList(Categorias.values()));
            return modelo;
        } catch (Exception execao) {
            return new ModelAndView(VISUALIZA_LISTAR_PERGUNTA);
        }
    }

    @PostMapping(MUDAR_ESTADO)
    public ModelAndView mudarEstadoPergunta(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {
        try {
            final ModelAndView modelo = new ModelAndView(REDIRECIONAR_PERGUNTA);
            final boolean existeId = perguntaServico.mudarEstado(id);
            if (existeId) {
                redirectAttrs.addFlashAttribute(MENSAGEM, ESTADO_MENSAGEM_SUCESSO);
            } else {
                redirectAttrs.addFlashAttribute(ERRO_MENSAGEM_PERGUNTA, ERRO_MUDAR_ESTADO);
            }
            return modelo;
        } catch (Exception excecao) {
            final ModelAndView modelo = new ModelAndView(REDIRECIONAR_PERGUNTA);
            redirectAttrs.addFlashAttribute(ERRO_MENSAGEM_PERGUNTA, ERRO);
            return modelo;
        }
    }

    @PostMapping(EXCLUIR_ID_PERGUNTA)
    @Transactional
    public ModelAndView excluirPerfil(@PathVariable Long id, RedirectAttributes atribRedirecionamento) {
        try {
            ModelAndView modelo = new ModelAndView(REDIRECIONAR_PERGUNTA);
            perguntaServico.excluir(id, atribRedirecionamento);
            return modelo;
        } catch (ServicoException excecao) {
            ModelAndView modelo = new ModelAndView(REDIRECIONAR_PERGUNTA);
            atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PERGUNTA, excecao);
            return modelo;
        } catch (Exception excecao) {
            ModelAndView modelo = new ModelAndView(REDIRECIONAR_PERGUNTA);
            atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PERGUNTA, ERRO);
            return modelo;
        }
    }

    @GetMapping(EDITAR_ID_PERGUNTA)
    public ModelAndView visualizarEditarPergunta(@PathVariable("id") Long id) {
        try {
            ModelAndView modelo = new ModelAndView(VISUALIZAR_EDITAR_PERGUNTA);
            Pergunta pergunta = this.perguntaServico.buscarPorId(id);
            modelo.addObject(pergunta);
            return modelo;
        } catch (Exception excecao) {
            return new ModelAndView(REDIRECIONAR_PERGUNTA);
        }
    }

    @PostMapping(EDITAR_ID_PERGUNTA)
    public ModelAndView editarPergunta(@Valid final Pergunta pergunta,
                                       final BindingResult resultado,
                                       final RedirectAttributes atribRedirecionamento) {
        try {
            if (!perguntaServico.validarPergunta(resultado)) {
                ModelAndView erro = new ModelAndView(REDIRECIONAR_EDITAR_PERGUNTA);
                erro.addObject(MODELO_PERGUNTA, pergunta);
                atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PERGUNTA, ERRO_EDITAR_PERGUNTA);
                return erro;
            } else {
                ModelAndView modelo = new ModelAndView(REDIRECIONAR_EDITAR_PERGUNTA);
                if (perguntaServico.editar(pergunta, atribRedirecionamento)) {
                    atribRedirecionamento.addFlashAttribute(MENSAGEM, EDITAR_MENSAGEM_SUCESSO);
                    return modelo;
                } else {
                    return modelo;
                }
            }
        } catch (Exception excecao) {

            ModelAndView modelo = new ModelAndView(REDIRECIONAR_EDITAR_PERGUNTA);
            modelo.addObject(MODELO_PERGUNTA, pergunta);
            atribRedirecionamento.addFlashAttribute(ERRO, ERRO_EDITAR_PERGUNTA);
            return modelo;
        }
    }

}

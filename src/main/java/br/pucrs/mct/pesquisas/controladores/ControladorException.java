package br.pucrs.mct.pesquisas.controladores;

public class ControladorException extends RuntimeException{
	private static final long serialVersionUID = -1978589047971982038L;
	
	public ControladorException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

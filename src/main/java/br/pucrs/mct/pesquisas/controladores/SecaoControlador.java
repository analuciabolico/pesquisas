package br.pucrs.mct.pesquisas.controladores;

import javax.validation.Valid;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Pergunta;
import br.pucrs.mct.pesquisas.modelos.Pesquisa;
import br.pucrs.mct.pesquisas.modelos.Secao;
import br.pucrs.mct.pesquisas.servicos.PerguntaServico;
import br.pucrs.mct.pesquisas.servicos.PesquisaServico;

@Controller
@RequestMapping("/pesquisas")
public class SecaoControlador {

    private static final String VISUALIZA_PESQUISA_EDITAR = "pesquisas/secao";
    private static final String PESQUISA_SECAO = "/secao";

    private final PesquisaServico pesquisaServico;
    private final PerguntaServico perguntaServico;

    public SecaoControlador(final PesquisaServico pesquisaServico,
                            final PerguntaServico perguntaServico) {
        this.pesquisaServico = pesquisaServico;
        this.perguntaServico = perguntaServico;
    }


    @PostMapping(PESQUISA_SECAO)
    public ModelAndView salvarPesquisa(@Valid final Pesquisa pesquisa,
                                       final List<Secao> secoes,
                                       final BindingResult resultado,
                                       final RedirectAttributes atribRedirecionamento) {
        try {
            if (!pesquisaServico.validarPesquisa(resultado)) {
                final ModelAndView erro = new ModelAndView(VISUALIZA_PESQUISA_EDITAR);
                erro.addObject("pesquisa", pesquisa);
                erro.addObject("secoes", secoes);
                return erro;
            } else {
                final ModelAndView modelo = new ModelAndView(VISUALIZA_PESQUISA_EDITAR);
                pesquisaServico.salvar(pesquisa, atribRedirecionamento);
                final List<Pergunta> perguntas = perguntaServico.buscarPorEstado(Estados.ATIVADA);
                modelo.addObject("pesquisa", pesquisa);
                modelo.addObject("perguntas", perguntas);
                return modelo;
            }
        } catch (Exception excecao) {
            final ModelAndView erroExcecao = new ModelAndView(VISUALIZA_PESQUISA_EDITAR);
            final List<Pergunta> perguntas = perguntaServico.buscarPorEstado(Estados.ATIVADA);
            erroExcecao.addObject("pesquisa", pesquisa);
            erroExcecao.addObject("perguntas", perguntas);
            return erroExcecao;
        }
    }

}

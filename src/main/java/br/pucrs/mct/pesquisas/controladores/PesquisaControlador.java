package br.pucrs.mct.pesquisas.controladores;

import javax.validation.Valid;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.modelos.Pergunta;
import br.pucrs.mct.pesquisas.modelos.Pesquisa;
import br.pucrs.mct.pesquisas.modelos.Secao;
import br.pucrs.mct.pesquisas.servicos.PerfilServico;
import br.pucrs.mct.pesquisas.servicos.PerguntaServico;
import br.pucrs.mct.pesquisas.servicos.PesquisaServico;
import br.pucrs.mct.pesquisas.servicos.ServicoException;

@Controller
@RequestMapping("/pesquisas")
public class PesquisaControlador {

    /**
     * Mapeamentos e Views
     */
    private static final String REDIRECIONAR_PESQUISA = "redirect:/pesquisas";
    private static final String REDIRECIONAR_NOVA_PESQUISA = "redirect:/pesquisas/nova";
    private static final String REDIRECIONAR_EDITAR_PESQUISA = "redirect:/pesquisas/{id}/editar";
    private static final String VISUALIZA_PESQUISAS_LISTAR = "pesquisas/listarPesquisa";
    private static final String VISUALIZA_PESQUISA_NOVA = "pesquisas/novaPesquisa";
    private static final String VISUALIZAR_PESQUISA_EDITAR = "pesquisas/editarPesquisa";
    private static final String EDITAR_ID_PESQUISA = "/{id}/editar";
    private static final String EXCLUIR_ID_PESQUISA = "/{id}/excluir";
    private static final String NOVA_PESQUISA = "/nova";
    private static final String MUDAR_ESTADO = "/{id}/mudarEstado";

    /**
     * Mensagens de erro para o model
     */
    private static final String ERRO_MENSAGEM_PESQUISA = "mensagemError";

    /**
     * Mensagens para o model
     */
    private static final String MENSAGEM = "mensagem";
    private static final String PESQUISA_MODELO = "pesquisas";
    private static final String PESQUISA_MODELO_PERFIL = "perfis";

    /**
     * Mensagens de erro
     */
    private static final String ERRO = "Erro na sua solicitação.";
    private static final String ERRO_PESQUISA_EXISTENTE = "Erro, já existe uma pesquisa com esse nome.";
    private static final String ERRO_EDITAR_PESQUISA = "Erro ao editar a pesquisa.";
    private static final String ERRO_MUDAR_ESTADO = "Erro ao mudar estado da pesquisa.";
    private static final String ERRO_PERFIL_VINCULADO = "Já existe uma pesquisa ativa para este perfil.";

    /**
     * Mensagens de sucesso
     */
    private static final String EDITAR_MENSAGEM_SUCESSO = "Pesquisa editada com sucesso.";
    private static final String ESTADO_MENSAGEM_SUCESSO = "Estado alterado com sucesso.";

    /**
     * Variáveis
     */
    private static final String PERFIL = "perfil";
    private static final String PESQUISA = "pesquisa";
    private static final String IDIOMA = "idioma";



    private final PesquisaServico pesquisaServico;
    private final PerfilServico perfilServico;
    private final PerguntaServico perguntaServico;

    public PesquisaControlador(final PesquisaServico pesquisaServico,
                               final PerfilServico perfilServico,
                               final PerguntaServico perguntaServico) {
        this.pesquisaServico = pesquisaServico;
        this.perfilServico = perfilServico;
        this.perguntaServico = perguntaServico;

    }

    @PostMapping(NOVA_PESQUISA)
    public ModelAndView salvarPesquisa(@Valid final Pesquisa pesquisa,
                                       final BindingResult resultado,
                                       final RedirectAttributes atribRedirecionamento) {
        try {
            pesquisa.setEstado(Estados.ATIVADA);
            if (!pesquisaServico.validarPesquisa(resultado)) {
                return novaPesquisa(pesquisa);
            } else {
				final ModelAndView modelo = new ModelAndView(REDIRECIONAR_NOVA_PESQUISA);
                pesquisaServico.salvar(pesquisa, atribRedirecionamento);
				final List<Perfil> perfis = perfilServico.buscarPorEstado(Estados.ATIVADA);
                modelo.addObject(PESQUISA_MODELO_PERFIL, perfis);

                return modelo;
            }
        } catch (Exception excecao) {
			final ModelAndView erro = new ModelAndView(REDIRECIONAR_NOVA_PESQUISA);
            erro.addObject(PESQUISA, pesquisa);
            atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, ERRO_PESQUISA_EXISTENTE);
            return erro;
        }
    }

    @GetMapping(NOVA_PESQUISA)
    public ModelAndView novaPesquisa(final Pesquisa pesquisa) {
        try {
			final ModelAndView modelo = new ModelAndView(VISUALIZA_PESQUISA_NOVA);
			final List<Perfil> perfis = perfilServico.buscarPorEstado(Estados.ATIVADA);
            modelo.addObject(IDIOMA, Arrays.asList(Idiomas.values()));
            modelo.addObject(PESQUISA_MODELO, pesquisa);
            modelo.addObject(PESQUISA_MODELO_PERFIL, perfis);
            return modelo;
        } catch (Exception execao) {
            return new ModelAndView(VISUALIZA_PESQUISA_NOVA);
        }
    }

    @GetMapping
    public ModelAndView mostrarListaPesquisas() {
        try {
			final ModelAndView modelo = new ModelAndView(VISUALIZA_PESQUISAS_LISTAR);
			final List<Pesquisa> pesquisasLista = this.pesquisaServico.buscar();
            modelo.addObject(PESQUISA_MODELO, pesquisasLista);
            return modelo;
        } catch (Exception execao) {
            return new ModelAndView(VISUALIZA_PESQUISAS_LISTAR);
        }

    }

    @PostMapping
    @Transactional
    public ModelAndView listarPesquisasComFiltro(@RequestParam("estado") final Estados estado,
                                                 @RequestParam("nome") final String nome,
                                                 @RequestParam("idioma") final Idiomas idioma) {
        try {
            final ModelAndView modelo = new ModelAndView(VISUALIZA_PESQUISAS_LISTAR);
            List<Pesquisa> pesquisas;
            if (estado == null) {
                if (nome.isBlank()) {
					if (idioma == null) {
						pesquisas = this.pesquisaServico.buscar();// Listar sem filtro nenhum
					} else {
						pesquisas = this.pesquisaServico.buscarPorIdioma(idioma);// Listra com filtro de idioma
					}
                } else {
					if (idioma == null) {
						pesquisas = this.pesquisaServico.buscarPorNome(nome);// Listar com filtro de nome
					} else {
						pesquisas = this.pesquisaServico.buscarPorNomePorIdioma(nome, idioma);// Listar com filtro de
					}
                    // nome e idioma
                }
            } else {
                if (nome.isBlank()) {
					if (idioma == null) {
						pesquisas = this.pesquisaServico.buscarPorEstados(estado);// Listar com filtro de Estado
					} else {
						pesquisas = this.pesquisaServico.buscarPorEstadosPorIdioma(estado, idioma);// Listar com filtro
					}
                    // de Estado e
                    // idioma
                } else {
					if (idioma == null) {
						pesquisas = this.pesquisaServico.buscarPorEstadosPorNome(estado, nome);// Listar com filtro de
					}
					// Estado e nome
					else {
						pesquisas = this.pesquisaServico.buscarPorEstadoPorNomePorIdioma(estado, nome, idioma);// Listar
					}
                    // com
                    // todos
                    // os
                    // filtros

                }
            }
            modelo.addObject(PESQUISA_MODELO, pesquisas);
            return modelo;
        } catch (Exception execao) {
            return new ModelAndView(VISUALIZA_PESQUISAS_LISTAR);
        }
    }

    @GetMapping(EDITAR_ID_PESQUISA)
    public ModelAndView visualizarEditarPesquisa(@PathVariable("id") final Long id) {
        try {
			final ModelAndView modelo = new ModelAndView(VISUALIZAR_PESQUISA_EDITAR);
			final List<Perfil> perfis = perfilServico.buscarPorEstado(Estados.ATIVADA);
			final Perfil perfil = pesquisaServico.buscarPerfilPorPesquisa(id);
			final Pesquisa pesquisa = this.pesquisaServico.buscarPorId(id);
            modelo.addObject(pesquisa);
            modelo.addObject(PESQUISA_MODELO_PERFIL, perfis);
            modelo.addObject(PERFIL, perfil);
            return modelo;
        } catch (Exception excecao) {
            return new ModelAndView(REDIRECIONAR_PESQUISA);
        }
    }

    @PostMapping(EDITAR_ID_PESQUISA)
    public ModelAndView editarPesquisa(@Valid final Pesquisa pesquisa,
                                       final BindingResult resultado,
                                       final RedirectAttributes atribRedirecionamento) {
        try {
            if (!pesquisaServico.validarPesquisa(resultado)) {
				final ModelAndView erro = new ModelAndView(REDIRECIONAR_EDITAR_PESQUISA);
                erro.addObject(PESQUISA_MODELO, pesquisa);
                atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, ERRO_EDITAR_PESQUISA);
                return erro;
            } else {
                ModelAndView modelo = new ModelAndView(REDIRECIONAR_EDITAR_PESQUISA);
                if (pesquisaServico.editar(pesquisa)) {
                    atribRedirecionamento.addFlashAttribute(MENSAGEM, EDITAR_MENSAGEM_SUCESSO);
					final List<Pergunta> perguntas = perguntaServico.buscarPorEstado(Estados.ATIVADA);
					final Perfil perfil = pesquisaServico.buscarPerfilPorPesquisa(pesquisa.getId());
					final Set<Secao> secoes = pesquisaServico.buscarSecoes(pesquisa.getId());
                    modelo.addObject(PESQUISA_MODELO, pesquisa);
                    modelo.addObject("perguntas", perguntas);
                    modelo.addObject("secoes", secoes);
                    modelo.addObject(PERFIL, perfil);
				} else {
                    atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, ERRO_PERFIL_VINCULADO);
				}
				return modelo;
			}

        } catch (Exception excecao) {
			final ModelAndView modelo = new ModelAndView(REDIRECIONAR_EDITAR_PESQUISA);
            modelo.addObject(PESQUISA_MODELO, pesquisa);
            atribRedirecionamento.addFlashAttribute(ERRO, ERRO_EDITAR_PESQUISA);
            return modelo;
        }
    }

    @PostMapping(MUDAR_ESTADO)
    public ModelAndView mudarEstadoPesquisa(@PathVariable("id") final Long id,
                                            final RedirectAttributes redirectAttrs) {
        try {
			final ModelAndView modelo = new ModelAndView(REDIRECIONAR_PESQUISA);
			final boolean existeId = pesquisaServico.mudarEstado(id);
            if (existeId) {
                redirectAttrs.addFlashAttribute(MENSAGEM, ESTADO_MENSAGEM_SUCESSO);
			} else {
                redirectAttrs.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, ERRO_MUDAR_ESTADO);
			}
			return modelo;
		} catch (Exception excecao) {
			final ModelAndView modelo = new ModelAndView(REDIRECIONAR_PESQUISA);
            redirectAttrs.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, ERRO);
            return modelo;
        }
    }



    @PostMapping(EXCLUIR_ID_PESQUISA)
    @Transactional
    public ModelAndView excluirPesquisas(@PathVariable final Long id,
                                         final RedirectAttributes atribRedirecionamento) {
        try {
            ModelAndView modelo = new ModelAndView(REDIRECIONAR_PESQUISA);
            pesquisaServico.excluir(id, atribRedirecionamento);
            return modelo;
        } catch (ServicoException excecao) {
            ModelAndView erro = new ModelAndView(REDIRECIONAR_PESQUISA);
            atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, excecao);
            return erro;
        } catch (Exception excecao) {
            ModelAndView erroExcecao = new ModelAndView(REDIRECIONAR_PESQUISA);
            atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PESQUISA, ERRO);
            return erroExcecao;
        }

    }

}

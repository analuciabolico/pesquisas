package br.pucrs.mct.pesquisas.controladores;

import javax.transaction.Transactional;
import javax.validation.Valid;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.exceptions.TemplateInputException;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.servicos.PerfilServico;
import br.pucrs.mct.pesquisas.servicos.ServicoException;
import br.pucrs.mct.pesquisas.util.PerfilUtil;

@Controller
@RequestMapping("/perfis")
public class PerfilControlador {
    private final PerfilServico perfilServico;

    public PerfilControlador(final PerfilServico perfilServico) {
        this.perfilServico = perfilServico;
    }

    @GetMapping
    public ModelAndView listarPerfil(final Perfil perfil, final RedirectAttributes atribRedirecionamento) {
        try {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.VISUALIZAR_LISTAR_PERFIL);
            List<Perfil> listaPerfil;
            if (perfil.getEstado() == null) {
                if (perfil.getNome() == null) {
                    if (perfil.getIdioma() == null) {
                        listaPerfil = perfilServico.buscar();// Sem filtros
                    } else {
                        listaPerfil = perfilServico.buscarPorIdioma(perfil.getIdioma());// Filtrando por idioma
                    }
                } else {
                    if (perfil.getIdioma() == null) {
                        listaPerfil = perfilServico.buscarPorNome(perfil.getNome());// Filtrando por nome
                    } else {
                        listaPerfil = perfilServico.buscarPorNomeEIdioma(
                                perfil.getNome(),
                                perfil.getIdioma());// Filtrando
                        // por
                        // nome
                        // e
                        // idioma
                    }

                }
            } else {
                if (perfil.getNome() == null) {
                    if (perfil.getIdioma() == null) {
                        listaPerfil = perfilServico.buscarPorEstado(perfil.getEstado());// Filtrando por estado
                    } else {
                        listaPerfil = perfilServico.buscarPorEstadoEIdioma(
                                perfil.getEstado(),
                                perfil.getIdioma());// Filtrando
                        // por
                        // estado
                        // e
                        // idioma
                    }
                } else {
                    if (perfil.getIdioma() == null) {
                        listaPerfil = perfilServico.buscarPorEstadoENome(
                                perfil.getEstado(),
                                perfil.getNome());// Filtrando
                        // por
                        // estado
                        // e
                        // nome
                    } else {
                        listaPerfil = perfilServico.buscarPorEstadoENomeEIdioma(perfil.getEstado(), perfil.getNome(),
                                                                                perfil.getIdioma());// Filtrando por estado, nome e idioma
                    }

                }
            }

            modelo.addObject(PerfilUtil.MODELO_PERFIL, listaPerfil);
            return modelo;

        } catch (final Exception excecao) {
            atribRedirecionamento.addFlashAttribute(PerfilUtil.MENSAGEM, PerfilUtil.ERRO_LISTAR_PERFIL);
            return new ModelAndView(PerfilUtil.VISUALIZAR_LISTAR_PERFIL);
        }
    }

    @PostMapping(PerfilUtil.EXCLUIR_ID_PERFIL)
    @Transactional
    public ModelAndView excluirPerfil(@PathVariable final Long id, final RedirectAttributes atribRedirecionamento) {
        try {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.REDIRECIONAR_PERFIL);
            perfilServico.excluir(id, atribRedirecionamento);
            return modelo;
        } catch (final ServicoException excecao) {
            final ModelAndView erro = new ModelAndView(PerfilUtil.REDIRECIONAR_PERFIL);
            atribRedirecionamento.addFlashAttribute(PerfilUtil.ERRO_MENSAGEM_PERFIL, excecao);
            return erro;
        } catch (final Exception excecao) {
            final ModelAndView erroExcecao = new ModelAndView(PerfilUtil.REDIRECIONAR_PERFIL);
            atribRedirecionamento.addFlashAttribute(PerfilUtil.ERRO_MENSAGEM_PERFIL, PerfilUtil.ERRO);
            return erroExcecao;
        }
    }

    /**
     * @param id
     * @param atribRedirecionamento
     * @return
     */
    @PostMapping(PerfilUtil.MUDAR_ESTADO)
    @Transactional
    public ModelAndView mudarEstadoPerfil(@PathVariable final Long id, final RedirectAttributes atribRedirecionamento) {
        try {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.REDIRECIONAR_PERFIL);
            if (perfilServico.mudarEstado(id)) {
                atribRedirecionamento.addFlashAttribute(PerfilUtil.MENSAGEM, PerfilUtil.ESTADO_MENSAGEM_SUCESSO);
            }
            return modelo;
        } catch (final Exception excecao) {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.REDIRECIONAR_PERFIL);
            atribRedirecionamento.addFlashAttribute(PerfilUtil.MENSAGEM, PerfilUtil.ERRO_MUDAR_ESTADO);
            return modelo;
        }
    }

    @GetMapping(PerfilUtil.EDITAR_ID_PERFIL)
    public ModelAndView visualizarEditarPerfil(@PathVariable final Long id) {
        try {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.VISUALIZAR_EDITAR_PERFIL);
            final Perfil perfil = perfilServico.buscarPorId(id);
            modelo.addObject("perfil", perfil);
            return modelo;
        } catch (final TemplateInputException excecaoTemplate) {
            return new ModelAndView(PerfilUtil.VISUALIZAR_LISTAR_PERFIL);
        } catch (final Exception excecao) {
            return new ModelAndView(PerfilUtil.REDIRECIONAR_PERFIL);
        }
    }

    @PostMapping(PerfilUtil.EDITAR_ID_PERFIL)
    public ModelAndView editarPerfil(@Valid final Perfil perfil,
                                     final BindingResult resultado,
                                     final RedirectAttributes atribRedirecionamento) {
        try {
            if (!perfilServico.validarPerfil(perfil, resultado)) {
                final ModelAndView teste = new ModelAndView("perfis/editarPerfil");
                teste.addObject(perfil);
                return teste;

            } else {
				final ModelAndView modelo = new ModelAndView(PerfilUtil.REDIRECIONAR_EDITAR_PERFIL);
                atribRedirecionamento.addFlashAttribute(PerfilUtil.MENSAGEM, PerfilUtil.EDITAR_MENSAGEM_SUCESSO);
                perfilServico.salvar(perfil);
                return modelo;
            }
        } catch (final Exception excecao) {
			final ModelAndView erroExcecao = new ModelAndView(PerfilUtil.REDIRECIONAR_EDITAR_PERFIL);
            erroExcecao.addObject(PerfilUtil.MODELO_PERFIL, perfil);
            atribRedirecionamento.addFlashAttribute(PerfilUtil.ERRO_PERFIL_DUPLICADO, PerfilUtil.ERRO_CRIAR_DUPLICADO);
            return erroExcecao;
        }
    }

    @PostMapping(PerfilUtil.NOVO_PERFIL)
    public ModelAndView novoPerfil(@Valid final Perfil perfil,
                                   final BindingResult resultado,
                                   final RedirectAttributes atribRedirecionamento) {
        try {
            perfil.setEstado(Estados.ATIVADA);
            if (!perfilServico.validarPerfil(perfil, resultado)) {
                return visualizarNovoPerfil(perfil);
            } else {
                final ModelAndView modelo = new ModelAndView(PerfilUtil.REDIRECIONAR_NOVO_PERFIL);
                perfilServico.salvar(perfil);
                atribRedirecionamento.addFlashAttribute(PerfilUtil.MENSAGEM, PerfilUtil.CRIAR_MENSAGEM_SUCESSO);
                return modelo;
            }
        } catch (final Exception excecao) {
            final ModelAndView erroExcecao = new ModelAndView(PerfilUtil.REDIRECIONAR_NOVO_PERFIL);
            atribRedirecionamento.addFlashAttribute(PerfilUtil.ERRO_PERFIL_DUPLICADO, PerfilUtil.ERRO_CRIAR_DUPLICADO);
            return erroExcecao;
        }
    }

    @GetMapping(PerfilUtil.NOVO_PERFIL)
    public ModelAndView visualizarNovoPerfil(final Perfil perfil) {
        try {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.VISUALIZAR_NOVO_PERFIL);
            modelo.addObject(PerfilUtil.MODELO_PERFIL, perfil);
            return modelo;
        } catch (final Exception excecao) {
            final ModelAndView modelo = new ModelAndView(PerfilUtil.VISUALIZAR_NOVO_PERFIL);
            modelo.addObject(PerfilUtil.MODELO_PERFIL, perfil);
            return modelo;
        }
    }

}

package br.pucrs.mct.pesquisas.servicos;

public class ServicoException extends RuntimeException {

	private static final long serialVersionUID = 4244881451243023367L;

	public ServicoException(final String mensagem) {
		super(mensagem);
	}
	
	public ServicoException(final String mensagem, final Throwable causa) {
        super(mensagem, causa);
    }

}

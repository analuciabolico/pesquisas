package br.pucrs.mct.pesquisas.servicos;

import java.util.List;

import org.springframework.stereotype.Service;

import br.pucrs.mct.pesquisas.modelos.Secao;
import br.pucrs.mct.pesquisas.repositorios.SecaoRepositorio;

@Service
public class SecaoServico {
    private final SecaoRepositorio secaoRepositorio;

    public SecaoServico(final SecaoRepositorio secaoRepositorio) {
        this.secaoRepositorio = secaoRepositorio;
    }

    public List<Secao> buscarTodasSecoes() {
        return secaoRepositorio.findAll();
    }

    public Secao salvar(final Secao secao) {
        final String name = secao.getNome();
        final String trimName = name.trim();
        secao.setNome(trimName);
        return this.secaoRepositorio.saveAndFlush(secao);
    }

}

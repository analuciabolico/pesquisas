package br.pucrs.mct.pesquisas.servicos;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.modelos.Categorias;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Pergunta;
import br.pucrs.mct.pesquisas.repositorios.PerguntaRepositorio;

@Service
public class PerguntaServico {

	/**
	 * Mapeamentos para o model
	 */
	private static final String MENSAGEM = "mensagem";
	private static final String ERRO_MENSAGEM = "mensagemError";

	/**
	 * Mensagens de sucesso
	 */
	private static final String EXCLUIR_MENSAGEM_SUCESSO = "Pergunta excluída com sucesso.";

	/**
	 * Mensagens de Erros
	 */
	private static final String ERRO_PERGUNTA = "Erro";
	private static final String PERGUNTA_NAO_ENCONTRADA = "Pergunta não encontrada.";
	private static final String ERRO = "Erro na sua solicitação.";
	private static final String ERRO_CATEGORIA = "Não é possível alterar a categoria.";

	@Autowired
	private PerguntaRepositorio perguntaRepositorio;

	public PerguntaServico(PerguntaRepositorio perguntaRepositorio) {
		this.perguntaRepositorio = perguntaRepositorio;
	}

	// Salvar
	public void salvar(Pergunta pergunta) {
		try {
			pergunta.setEnunciado(pergunta.getEnunciado().trim());
			perguntaRepositorio.saveAndFlush(pergunta);
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarTodasPerguntas() {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findAllByOrderByEnunciadoAsc();
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public Pergunta buscarPorId(Long id) {
		try {
			Optional<Pergunta> pergunta = perguntaRepositorio.findById(id);
			if (pergunta.isPresent()) {
				return pergunta.get();
			} else {
				throw new ServicoException(PERGUNTA_NAO_ENCONTRADA);
			}
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEnunciado(String enunciado) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEnunciadoIgnoreCaseContainingOrderByEnunciadoAsc(enunciado);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}

	}

	public List<Pergunta> buscarPorIdioma(Idiomas idioma) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByIdiomaOrderByEnunciadoAsc(idioma);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorCategoria(Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByCategoriaOrderByEnunciadoAsc(categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstado(Estados estado) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoOrderByEnunciadoAsc(estado);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorIdiomaECategoria(Idiomas idioma, Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByIdiomaAndCategoriaOrderByEnunciadoAsc(idioma, categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEnunciadoECategoria(String enunciado, Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio
					.findByEnunciadoIgnoreCaseContainingAndCategoriaOrderByEnunciadoAsc(enunciado, categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEnunciadoEIdioma(String enunciado, Idiomas idioma) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEnunciadoIgnoreCaseContainingAndIdiomaOrderByEnunciadoAsc(enunciado,
					idioma);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEnunciadoEEstado(String enunciado, Estados estado) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEnunciadoIgnoreCaseContainingAndEstadoOrderByEnunciadoAsc(enunciado,
					estado);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoEIdioma(Estados estado, Idiomas idioma) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoAndIdiomaOrderByEnunciadoAsc(estado, idioma);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoECategoria(Estados estado, Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoAndCategoriaOrderByEnunciadoAsc(estado, categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoEIdiomaEEnunciado(Estados estado, Idiomas idioma, String enunciado) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoAndIdiomaAndEnunciadoIgnoreCaseContainingOrderByEnunciadoAsc(
					estado, idioma, enunciado);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoEIdiomaECategoria(Estados estado, Idiomas idioma, Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoAndIdiomaAndCategoriaOrderByEnunciadoAsc(estado, idioma,
					categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoEEnunciadoECategoria(Estados estado, String enunciado, Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoAndEnunciadoIgnoreCaseContainingAndCategoriaOrderByEnunciadoAsc(
					estado, enunciado, categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoEEnunciadoEIdioma(Estados estado, String enunciado, Idiomas idioma) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEstadoAndEnunciadoIgnoreCaseContainingAndIdiomaOrderByEnunciadoAsc(
					estado, enunciado, idioma);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEnunciadoEIdiomaECategoria(String enunciado, Idiomas idioma, Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio.findByEnunciadoIgnoreCaseContainingAndIdiomaAndCategoriaOrderByEnunciadoAsc(
					enunciado, idioma, categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEnunciadoEIdiomaECategoriaEEstado(String enunciado, Idiomas idioma,
			Categorias categoria, Estados estado) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio
					.findByEnunciadoIgnoreCaseContainingAndIdiomaAndCategoriaAndEstadoOrderByEnunciadoAsc(enunciado,
							idioma, categoria, estado);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public List<Pergunta> buscarPorEstadoEEnunciadoEIdiomaECategoria(Estados estado, String enunciado, Idiomas idioma,
			Categorias categoria) {
		try {
			List<Pergunta> perguntas;
			perguntas = perguntaRepositorio
					.findByEstadoAndEnunciadoIgnoreCaseContainingAndIdiomaAndCategoriaOrderByEnunciadoAsc(estado,
							enunciado, idioma, categoria);
			return perguntas;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public Boolean mudarEstado(Long id) {

		try {

			Optional<Pergunta> pergunta = perguntaRepositorio.findById(id);

			if (pergunta.isPresent() && pergunta.get().getEstado() == Estados.ATIVADA) {
				pergunta.get().setEstado(Estados.DESATIVADA);
				perguntaRepositorio.saveAndFlush(pergunta.get());
				return true;
			}

			else if (pergunta.isPresent() && pergunta.get().getEstado() == Estados.DESATIVADA) {
				pergunta.get().setEstado(Estados.ATIVADA);
				perguntaRepositorio.saveAndFlush(pergunta.get());
				return true;
			} else {
				return false;
			}

		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}

	public boolean editar(@Valid Pergunta pergunta, RedirectAttributes atribRedirecionamento) {
		try {
			boolean test = true;
			Optional<Pergunta> perguntaAntiga = perguntaRepositorio.findById(pergunta.getId());
			if(perguntaAntiga.isPresent()) {
				if (pergunta.getCategoria() != perguntaAntiga.get().getCategoria()) {
					atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM, ERRO_CATEGORIA);
					test = false;
				}else {
					perguntaRepositorio.saveAndFlush(pergunta);
				}
			}
			return test;
		} catch (Exception excecao) {
			throw new ServicoException(PERGUNTA_NAO_ENCONTRADA, excecao);
		}
	}

	public void excluir(Long id, RedirectAttributes atribRedirecionamento) {
		try {
			Optional<Pergunta> pergunta = perguntaRepositorio.findById(id);
			if (pergunta.isPresent()) {
				perguntaRepositorio.delete(pergunta.get());
				atribRedirecionamento.addFlashAttribute(MENSAGEM, EXCLUIR_MENSAGEM_SUCESSO);
			}
		} catch (Exception excecao) {
			throw new ServicoException(ERRO, excecao);
		}
	}
	
	public Boolean validarPergunta(BindingResult resultado) {
		try {
			boolean test = true;
				if (resultado.hasErrors()) {
					test = false;
				}
				return test;
		} catch (Exception excecao) {
			throw new ServicoException(ERRO_PERGUNTA, excecao);
		}
	}
}

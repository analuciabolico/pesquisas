package br.pucrs.mct.pesquisas.servicos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.modelos.Pesquisa;
import br.pucrs.mct.pesquisas.modelos.Secao;
import br.pucrs.mct.pesquisas.repositorios.PesquisaRepositorio;
import br.pucrs.mct.pesquisas.util.PesquisaUtil;

@Service
public class PesquisaServico {
    private final PesquisaRepositorio pesquisaRepositorio;
    private final SecaoServico secaoServico;

    public PesquisaServico(final PesquisaRepositorio pesquisaRepositorio, final SecaoServico secaoServico) {
        this.pesquisaRepositorio = pesquisaRepositorio;
        this.secaoServico = secaoServico;
    }

    public void salvar(final Pesquisa pesquisa, final RedirectAttributes atribRedirecionamento) {
        try {
            pesquisa.setNome(pesquisa.getNome()
                                     .trim());

            if (pesquisa.getPerfil() != null) {
                List<Pesquisa> pesquisaList = pesquisaRepositorio.findByPerfil(pesquisa.getPerfil());
                if (!pesquisaList.isEmpty()) {

                    atribRedirecionamento.addFlashAttribute(PesquisaUtil.ERRO_PESQUISA, PesquisaUtil.ERRO_PERFIL_VINCULADO);
                } else {
                    this.pesquisaRepositorio.saveAndFlush(pesquisa);
                    atribRedirecionamento.addFlashAttribute(PesquisaUtil.PESQUISA_MENSAGEM, PesquisaUtil.PESQUISA_SALVA_SUCESSO);
                }
            } else {
                this.pesquisaRepositorio.saveAndFlush(pesquisa);
                atribRedirecionamento.addFlashAttribute(PesquisaUtil.PESQUISA_MENSAGEM, PesquisaUtil.PESQUISA_SALVA_SUCESSO);
            }
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO_SALVAR_PESQUISA, excecao);
        }

    }

    public Pesquisa buscarPorId(final Long id) {
        try {
            Optional<Pesquisa> pesquisa = pesquisaRepositorio.findById(id);
            if (pesquisa.isPresent()) {
                return pesquisa.get();
            } else {
                throw new ServicoException(PesquisaUtil.PESQUISA_NAO_ENCONTRADA);
            }
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscar() {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findAll();
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorEstados(final Estados estado) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByEstadoOrderByNomeAsc(estado);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorNome(final String nome) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByNomeIgnoreCaseContainingOrderByNomeAsc(nome);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorIdioma(final Idiomas idioma) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByIdiomaOrderByNomeAsc(idioma);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorEstadosPorIdioma(final Estados estado, final Idiomas idioma) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByEstadoAndIdiomaOrderByNomeAsc(estado, idioma);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorEstadosPorNome(final Estados estado, final String nome) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByEstadoAndNomeIgnoreCaseContainingOrderByNomeAsc(estado, nome);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorNomePorIdioma(final String nome, final Idiomas idioma) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(nome, idioma);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public List<Pesquisa> buscarPorEstadoPorNomePorIdioma(final Estados estado,
                                                          final String nome,
                                                          final Idiomas idioma) {
        try {
            List<Pesquisa> pesquisas;
            pesquisas = this.pesquisaRepositorio.findByEstadoAndNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(
                    estado,
                    nome,
                    idioma);
            return pesquisas;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public Perfil buscarPerfilPorPesquisa(final Long id) {
        try {
            Perfil perfil = null;
            Optional<Pesquisa> pesquisa = pesquisaRepositorio.findById(id);
            if (pesquisa.isPresent()) {
                perfil = pesquisa.get()
                                 .getPerfil();
            }
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public boolean editar(final Pesquisa pesquisa) {
        try {
            boolean test = true;
            Optional<Pesquisa> pesquisaAntiga = pesquisaRepositorio.findById(pesquisa.getId());
            if (pesquisaAntiga.isPresent()) {
                if (pesquisa.getEstado() != null) {
                    if (pesquisa.getPerfil() != null) {
                        List<Pesquisa> pesquisaList = pesquisaRepositorio.findByPerfil(pesquisa.getPerfil());
                        if (pesquisaList.isEmpty()) {
                            pesquisa.setNome(pesquisa.getNome()
                                                     .trim());
                            pesquisaRepositorio.saveAndFlush(pesquisa);
                            test = true;
                        } else {
                            if (pesquisaList.contains(pesquisaAntiga.get())) {
                                pesquisa.setNome(pesquisa.getNome()
                                                         .trim());
                                pesquisaRepositorio.saveAndFlush(pesquisa);
                                test = true;
                            } else {
                                test = false;
                            }
                        }
                    } else {
                        pesquisa.setNome(pesquisa.getNome()
                                                 .trim());
                        pesquisaRepositorio.saveAndFlush(pesquisa);
                        test = true;
                    }
                } else {
                    test = false;
                }

            } else {
                test = false;
            }
            return test;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.PESQUISA_NAO_ENCONTRADA, excecao);
        }
    }

    public Boolean mudarEstado(final Long id) {

        try {

            Optional<Pesquisa> pesquisa = pesquisaRepositorio.findById(id);

            if (pesquisa.isPresent() &&
                    pesquisa.get()
                            .getEstado() == Estados.ATIVADA) {
                pesquisa.get()
                        .setEstado(Estados.DESATIVADA);
                pesquisaRepositorio.saveAndFlush(pesquisa.get());
                return true;
            } else if (pesquisa.isPresent() &&
                    pesquisa.get()
                            .getEstado() == Estados.DESATIVADA) {
                pesquisa.get()
                        .setEstado(Estados.ATIVADA);
                pesquisaRepositorio.saveAndFlush(pesquisa.get());
                return true;
            } else {
                return false;
            }

        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO, excecao);
        }
    }

    public void excluir(final Long id, final RedirectAttributes atribRedirecionamento) {
        try {
            Optional<Pesquisa> pesquisa = pesquisaRepositorio.findById(id);
            if (pesquisa.isPresent()) {
                pesquisaRepositorio.delete(pesquisa.get());
                atribRedirecionamento.addFlashAttribute(PesquisaUtil.PESQUISA_MENSAGEM, PesquisaUtil.EXCLUIR_MENSAGEM_SUCESSO);
            }
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO_EXCLUSAO, excecao);
        }
    }

    public boolean validarPesquisa(final BindingResult resultado) {
        try {
            return !resultado.hasErrors();
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO_PESQUISA, excecao);
        }
    }

    public Set<Secao> buscarSecoes(final Long id) {
        try {
            Set<Secao> secoes = null;
            final Optional<Pesquisa> pesquisa = pesquisaRepositorio.findById(id);
            if (pesquisa.isPresent()) {
                secoes = pesquisa.get()
                                 .getSecoes();
            } else {
                Secao secao = new Secao("secaoTest", 1, "section1", null, pesquisa.get());
                secao = secaoServico.salvar(secao);
                secoes.add(secao);
            }
            return secoes;
        } catch (Exception excecao) {
            throw new ServicoException(PesquisaUtil.ERRO_EXCLUSAO, excecao);
        }
    }

}

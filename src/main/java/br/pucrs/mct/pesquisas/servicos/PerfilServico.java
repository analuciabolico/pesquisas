package br.pucrs.mct.pesquisas.servicos;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.modelos.Pesquisa;
import br.pucrs.mct.pesquisas.repositorios.PerfilRepositorio;
import br.pucrs.mct.pesquisas.repositorios.PesquisaRepositorio;

@Service
public class PerfilServico {

    /**
     * Mensagens para o model
     */
    private static final String MENSAGEM = "mensagem";

    /**
     * Mensagens de sucesso
     */
    private static final String EXCLUIR_PERFIL = "Perfil excluído.";

    /**
     * Mensagens de erro para o model
     */
    private static final String ERRO_MENSAGEM_PERFIL = "mensagemError";

    /**
     * Mensagens de Erros
     */
    private static final String ERRO_PERFIL = "Erro";
    private static final String ERRO_BUSCAR_PERFIS = "Erro ao listar os Perfis.";
    private static final String ERRO_BUSCAR_NOME_IDIOMA = "Erro ao buscar Perfis por nome e idioma.";
    private static final String ERRO_BUSCAR_IDIOMA = "Erro ao buscar perfis por idioma.";
    private static final String ERRO_BUSCAR_NOME = "Erro ao buscar Perfil por nome.";
    private static final String ERRO_PERFIL_NAO_ENCONTRADO = "Perfil não encontrado.";
    private static final String ERRO_BUSCAR_ID = "Erro ao buscar perfil por id.";
    private static final String ERRO_SALVAR_PERFIL = "Erro ao salvar Perfil.";
    private static final String ERRO_ESTADO_PERFIL = "Erro ao mudar estado.";
    private static final String ERRO_EXCLUIR_PERFIL = "Erro ao excluir o perfil.";
    private static final String ERRO_IDADE_PERFIL = "Erro a idade mínima não pode ser maior ou igual a idade máxima.";
    private static final String ERRO_EXCLUIR_PERFIL_VINCULADO =
            "Erro, esse perfil já está está vinculado a uma pesquisa.";


    private final PerfilRepositorio perfilRepositorio;

    private final PesquisaRepositorio pesquisaRepositorio;

    public PerfilServico(PerfilRepositorio perfilRepositorio, PesquisaRepositorio pesquisaRepositorio) {
        super();
        this.perfilRepositorio = perfilRepositorio;
        this.pesquisaRepositorio = pesquisaRepositorio;
    }

    /**
     * @param perfil
     */
    public void salvar(Perfil perfil) {
        try {
            perfil.setNome(perfil.getNome()
                                 .trim());
            this.perfilRepositorio.saveAndFlush(perfil);
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_SALVAR_PERFIL, excecao);
        }

    }

    /**
     * @return List<Perfil> contendo todos os "Perfis"
     */
    public List<Perfil> buscar() {
        try {
            List<Perfil> perfil;
            perfil = perfilRepositorio.findAllByOrderByNomeAsc();
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_PERFIS, excecao);
        }
    }

    public List<Perfil> buscarPorEstado(Estados ativa) {
        try {
            List<Perfil> perfil;
            perfil = perfilRepositorio.findByEstadoOrderByNomeAsc(ativa);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_PERFIS, excecao);
        }

    }

    public List<Perfil> buscarPorIdioma(Idiomas idioma) {
        try {
            List<Perfil> perfil;
            perfil = perfilRepositorio.findByIdiomaOrderByNomeAsc(idioma);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_IDIOMA, excecao);
        }

    }

    public List<Perfil> buscarPorNome(String nome) {
        try {
            List<Perfil> perfil;
            perfil = perfilRepositorio.findByNomeIgnoreCaseContainingOrderByNomeAsc(nome);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_NOME, excecao);
        }

    }

    public List<Perfil> buscarPorNomeEIdioma(String nome, Idiomas idioma) {
        try {
            List<Perfil> perfil;
            perfil = perfilRepositorio.findByNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(nome, idioma);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_NOME_IDIOMA, excecao);
        }

    }

    public List<Perfil> buscarPorEstadosEIdioma(Estados ativa, Idiomas idioma) {
        try {
            List<Perfil> perfil;
            perfil = this.perfilRepositorio.findByEstadoAndIdiomaOrderByNomeAsc(ativa, idioma);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_PERFIS, excecao);
        }
    }

    public List<Perfil> buscarPorEstadosPorNome(Estados ativa, String nome) {
        try {
            List<Perfil> perfil;
            perfil = this.perfilRepositorio.findByEstadoAndNomeIgnoreCaseContainingOrderByNomeAsc(ativa, nome);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_NOME, excecao);
        }
    }

    public List<Perfil> buscarPorEstadoENomeEIdioma(Estados estado, String nome, Idiomas idioma) {
        try {
            List<Perfil> perfil;
            perfil = this.perfilRepositorio.findByEstadoAndNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(estado, nome,
                                                                                                           idioma);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_NOME_IDIOMA, excecao);
        }
    }

    public List<Perfil> buscarPorEstadoEIdioma(Estados estado, Idiomas idioma) {
        try {
            List<Perfil> perfil;
            perfil = this.perfilRepositorio.findByEstadoAndIdiomaOrderByNomeAsc(estado, idioma);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_NOME_IDIOMA, excecao);
        }
    }

    public List<Perfil> buscarPorEstadoENome(Estados estado, String nome) {
        try {
            List<Perfil> perfil;
            perfil = this.perfilRepositorio.findByEstadoAndNomeIgnoreCaseContainingOrderByNomeAsc(estado, nome);
            return perfil;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_NOME_IDIOMA, excecao);
        }
    }

    /**
     * @param id
     * @return um Perfil
     *
     * @throws Caso ele nao encontre o id ou acontece algum erro ele lança uma ServicoException
     */
    public Perfil buscarPorId(Long id) {
        try {
            Optional<Perfil> perfil = perfilRepositorio.findById(id);
            if (perfil.isPresent()) {
                return perfil.get();
            } else {
                throw new ServicoException(ERRO_PERFIL_NAO_ENCONTRADO);
            }

        } catch (IllegalArgumentException excecao) {
            throw new ServicoException(ERRO_BUSCAR_ID, excecao);
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_BUSCAR_ID, excecao);
        }

    }

    public boolean mudarEstado(Long id) {
        try {
            Optional<Perfil> perfil = perfilRepositorio.findById(id);
            if (perfil.isPresent() &&
                    perfil.get()
                          .getEstado() == Estados.ATIVADA) {
                perfil.get()
                      .setEstado(Estados.DESATIVADA);
                perfilRepositorio.saveAndFlush(perfil.get());
                return true;
            } else if (perfil.isPresent() &&
                    perfil.get()
                          .getEstado() == Estados.DESATIVADA) {
                perfil.get()
                      .setEstado(Estados.ATIVADA);
                perfilRepositorio.saveAndFlush(perfil.get());
                return true;
            } else {
                return false;
            }
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_ESTADO_PERFIL, excecao);
        }
    }

    public void excluir(Long id, RedirectAttributes atribRedirecionamento) {
        try {
            Optional<Perfil> perfil = perfilRepositorio.findById(id);

            if (perfil.isPresent()) {
                List<Pesquisa> lista = pesquisaRepositorio.findByPerfil(perfil.get());
                if (lista.isEmpty()) {
                    perfilRepositorio.delete(perfil.get());
                    atribRedirecionamento.addFlashAttribute(MENSAGEM, EXCLUIR_PERFIL);
                } else {
                    atribRedirecionamento.addFlashAttribute(ERRO_MENSAGEM_PERFIL, ERRO_EXCLUIR_PERFIL_VINCULADO);
                }
            }
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_EXCLUIR_PERFIL, excecao);
        }

    }

    public boolean validarPerfil(Perfil perfil, BindingResult resultado) {
        try {
            boolean test = !resultado.hasErrors();

			if (perfil.getMinIdade() >= perfil.getMaxIdade()) {
				final ObjectError erro = new ObjectError(ERRO_MENSAGEM_PERFIL, ERRO_IDADE_PERFIL);
                resultado.addError(erro);
                test = false;
            }
            return test;
        } catch (Exception excecao) {
            throw new ServicoException(ERRO_PERFIL, excecao);
        }

    }

}

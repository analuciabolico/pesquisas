package br.pucrs.mct.pesquisas.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PERFIS")
public class Perfil extends EntidadeBase {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Nome obrigatório.")
	@Column(nullable = false, name = "NOME", length = 50, unique = true)
	@Size(max = 50, message = "Tamanho mínimo de caracteres é 1 e no máximo 50 caracteres.")
	private String nome;
	
	@Min(0)
	@Max(119)
	@Column(nullable = false, name = "MIN_IDADE")
	@NotNull(message = "Campo idade mínima obrigatório.")
	@PositiveOrZero(message = "Campo idade mínima não pode ser menor do que 0 anos.")
	private int minIdade;

	
	@Min(0)
	@Max(120)
	@Column(nullable = false, name = "MAX_IDADE")
	@NotNull(message = "Campo idade máxima obrigatório.")
	@PositiveOrZero(message = "Campo idade máxima não pode ser menor do que 0 anos.")
	private int maxIdade;

	@NotNull(message = "Campo idioma obrigatório.")
	@Column(nullable = false, name = "IDIOMA")
	@Enumerated(EnumType.STRING)
	private Idiomas idioma;

	@Column(nullable = false, name = "ATIVA")
	@Enumerated(EnumType.STRING)
	private Estados estado;
	
	@OneToOne
	@JoinColumn(name = "PESQUISA")
	private Pesquisa pesquisa;

	public Perfil() {}	
	public Perfil(
			@NotBlank(message = "Nome obrigatório.") @Size(max = 50, message = "Tamanho mínimo de caracteres é 1 e no máximo 50 caracteres.") String nome,
			@NotNull(message = "Campo idade mínima obrigatório.") @Min(0) @Max(119) @PositiveOrZero(message = "Campo idade mínima não pode ser menor do que 0 anos.") int minIdade,
			@NotNull(message = "Campo idade máxima obrigatório.") @Max(120) @Min(0) @PositiveOrZero(message = "Campo idade máxima não pode ser menor do que 0 anos.") int maxIdade,
			@NotNull(message = "Campo idioma obrigatório.") Idiomas idioma, Estados estado) {
		super();
		this.nome = nome;
		this.minIdade = minIdade;
		this.maxIdade = maxIdade;
		this.idioma = idioma;
		this.estado = estado;
	}


	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getMinIdade() {
		return minIdade;
	}

	public void setMinIdade(int minIdade) {
		this.minIdade = minIdade;
	}

	public int getMaxIdade() {
		return maxIdade;
	}

	public void setMaxIdade(int maxIdade) {
		this.maxIdade = maxIdade;
	}

	public Idiomas getIdioma() {
		return idioma;
	}

	public void setIdioma(Idiomas idioma) {
		this.idioma = idioma;
	}

	public Pesquisa getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(final Pesquisa pesquisa) {
		this.pesquisa = pesquisa;
	}

	@Override
	public String toString() {
		return "Perfil [nome=" + nome + ", min_idade=" + minIdade + ", max_idade=" + maxIdade + ", idioma=" + idioma
				+ ", estado=" + estado + ", id=" + id + ", isNew()=" + isNew() + "]";
	}

}

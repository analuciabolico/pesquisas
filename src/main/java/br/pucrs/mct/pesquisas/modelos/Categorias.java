package br.pucrs.mct.pesquisas.modelos;

public enum Categorias {
	
	DISSERTATIVA("Dissertativa");
	
	private String descricao;
	
	private Categorias(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}

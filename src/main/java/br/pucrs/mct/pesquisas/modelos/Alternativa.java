package br.pucrs.mct.pesquisas.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "ALTERNATIVAS")
public class Alternativa extends EntidadeBase {
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "Legenda obrigatório")
	@Size(max = 50, message = "Tamanho mínimo de caracteres é 1 e no máximo 50 caracteres.")
	@Column
	private String legenda;
	
	@NotNull(message = "Idioma obrigatório")
	@Column
	private Boolean idioma;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "perguntas_Fk", nullable = false)
	private Pergunta perguntas;
	
	public Alternativa() {}
	public Alternativa(@NotBlank(message = "Legenda obrigatório") String legenda,
			@NotNull Boolean idioma, Pergunta perguntas) {
		super();
		this.legenda = legenda;
		this.idioma = idioma;
		this.perguntas = perguntas;
	}

	public String getLegenda() {
		return legenda;
	}

	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}

	public Boolean getIdioma() {
		return idioma;
	}

	public void setIdioma(Boolean idioma) {
		this.idioma = idioma;
	}

	public Pergunta getPergunta() {
		return perguntas;
	}

	public void setPergunta(Pergunta perguntas) {
		this.perguntas = perguntas;
	}

}

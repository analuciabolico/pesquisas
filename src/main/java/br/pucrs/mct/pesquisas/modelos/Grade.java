package br.pucrs.mct.pesquisas.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "GRADES")
public class Grade extends EntidadeBase {
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "Enunciado não pode estar vazio ou nulo!")
	@Column
	private String enunciado;
	@NotEmpty
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "perguntas_Fk", nullable = false)
	private Pergunta perguntas;
	
	public Grade() {}
	public Grade(@NotBlank(message = "Enunciado não pode estar vazio ou nulo!") String enunciado, @NotEmpty Pergunta perguntas) {
		super();
		this.enunciado = enunciado;
		this.perguntas = perguntas;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public Pergunta getPergunta() {
		return perguntas;
	}

	public void setPergunta(Pergunta perguntas) {
		this.perguntas = perguntas;
	}
}

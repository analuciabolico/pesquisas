package br.pucrs.mct.pesquisas.modelos;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PESQUISAS")
public class Pesquisa extends EntidadeBase {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Nome obrigatório")
	@Column(nullable = false,name = "NOME", length = 100, unique = true)
	@Size(max = 100, message = "Quantidade de caracteres para o nome da pesquisa é inválido. Máximo de 100 caracteres.")
	private String nome;

	@OneToOne
	@JoinColumn(name = "PERFIL")
	private Perfil perfil;

	@NotNull(message = "Idioma obrigatório")
	@Column(nullable = false, name = "IDIOMA")
	@Enumerated(EnumType.STRING)
	private Idiomas idioma;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, name = "ESTADO")
	private Estados estado;

	@OneToMany(mappedBy = "pesquisa")

	private Set<Secao> secoes = new HashSet<>();
	
	public Pesquisa() {
	}

	public Pesquisa(
			@NotBlank(message = "Nome obrigatório") @Size(max = 100, message = "Quantidade de caracteres para o nome da pesquisa é inválido. Máximo de 100 caracteres.") String nome,
			Perfil perfil, @NotNull(message = "Idioma obrigatório") Idiomas idioma, Estados estado, Set<Secao> secoes) {
		super();
		this.nome = nome;
		this.perfil = perfil;
		this.idioma = idioma;
		this.estado = estado;
		this.secoes = secoes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Idiomas getIdioma() {
		return idioma;
	}

	public void setIdioma(Idiomas idioma) {
		this.idioma = idioma;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public Set<Secao> getSecoes() {
		return secoes;
	}

	public void setSecoes(Set<Secao> secoes) {
		this.secoes = secoes;
	}

	@Override
	public String toString() {
		return "Pesquisa [nome=" + nome + ", perfil=" + perfil + ", idioma=" + idioma + ", estado=" + estado + ", secoes="
				+ secoes + ", id=" + id + ", isNew()=" + isNew() + "]";
	}

}

package br.pucrs.mct.pesquisas.modelos;

public enum Idiomas {
	
	PORTUGUES("Português"),
	INGLES("Inglês");
	
	private String descricao;
	
	private Idiomas(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}

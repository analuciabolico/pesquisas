package br.pucrs.mct.pesquisas.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "SECOES")
public class Secao extends EntidadeBase {
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "É obrigatório preencher um nome para a seção.")
	@Column(nullable = false, name = "NOME")
	@Size(min = 5, max = 50, message = "Tamanho de caracteres inválido. Mínimo 5 e máximo 50.")
	private String nome;
	
	@NotEmpty(message = "É obrigatório selecionar uma seção")
	@Column(nullable = false, name = "PAGINACAO")
	@Range(min = 1, max = 10)
	private Integer paginacao;

	@NotBlank(message = "Descrição é obrigatório")
	@Column(nullable = false, name = "DESCRICAO", length = 255)
	@Size(min = 10, max = 255, message = "Tamanho de caracteres inválido. Mínimo 10 e máximo 255.")
	private String descricao;
	
	@OneToOne
	@JoinColumn(name = "pergunta_fk")
	private Pergunta pergunta;
	
	@ManyToOne
    @JoinColumn(name="pesquisa_fk", nullable=false)
	private Pesquisa pesquisa;
	
	public Secao() {
	}

	public Secao(
			@NotBlank(message = "É obrigatório preencher um nome para a seção.") @Size(min = 5, max = 50, message = "Tamanho de caracteres inválido. Mínimo 5 e máximo 50.") String nome,
			@NotEmpty(message = "É obrigatório selecionar uma seção") @Range(min = 1, max = 10) Integer paginacao,
			@NotBlank(message = "Descrição é obrigatório") @Size(min = 10, max = 255, message = "Tamanho de caracteres inválido. Mínimo 10 e máximo 255.") String descricao,
			Pergunta pergunta, Pesquisa pesquisa) {
		super();
		this.nome = nome;
		this.paginacao = paginacao;
		this.descricao = descricao;
		this.pergunta = pergunta;
		this.pesquisa = pesquisa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPaginacao() {
		return paginacao;
	}

	public void setPaginacao(Integer paginacao) {
		this.paginacao = paginacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}
	public Pesquisa getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(Pesquisa pesquisa) {
		this.pesquisa = pesquisa;
	}

	@Override
	public String toString() {
		return "Secao [nome=" + nome + ", paginacao=" + paginacao + ", descricao=" + descricao + ", pergunta="
				+ pergunta + ", id=" + id + ", isNew()=" + isNew() + "]";
	}


}

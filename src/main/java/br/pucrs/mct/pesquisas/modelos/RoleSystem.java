package br.pucrs.mct.pesquisas.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "ROLES")
public class RoleSystem implements GrantedAuthority {
	@Id
	@SequenceGenerator(name = "seq_roles", sequenceName = "SEQUENCE_ROLES", allocationSize = 1)
	@GeneratedValue(generator = "seq_roles")
	private Integer id;
	private String emailSetor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSetor() {
		return emailSetor;
	}

	public void setNome(String setor) {
		this.emailSetor = setor;
	}

	@Override
	public String getAuthority() {
		return getSetor();
	}

}

package br.pucrs.mct.pesquisas.modelos;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PERGUNTAS")
public class Pergunta extends EntidadeBase {
	private static final long serialVersionUID = 1L;

	@Column(nullable = false, name = "ENUNCIADO", length = 150)
	@NotBlank(message = "Enunciado Obrigatório")
	private String enunciado;

	@NotNull(message = "Idioma Obrigatório")
	@Column(nullable = false, name = "IDIOMA")
	@Enumerated(EnumType.STRING)
	private Idiomas idioma;

	@NotNull(message = "Categoria Obrigatório")
	@Column(nullable = false, name = "CATEGORIA")
	@Enumerated(EnumType.STRING)
	private Categorias categoria;

	@Column(nullable = true, name = "IMAGEM")
	private String imagem;

	@Column(nullable = false, name = "ESTADO")
	@Enumerated(EnumType.STRING)
	private Estados estado;

	@OneToMany(mappedBy = "perguntas")
	private List<Alternativa> alternativas;

	@OneToMany(mappedBy = "perguntas")
	private List<Grade> grades;

	public Pergunta() {
	}

	public Pergunta(@NotBlank(message = "Enunciado Obrigatório") String enunciado,
			@NotNull(message = "Idioma Obrigatório") Idiomas idioma,
			@NotNull(message = "Categoria Obrigatório") Categorias categoria, String imagem, @NotNull Estados estado,
			List<Alternativa> alternativas, List<Grade> grades) {
		super();
		this.enunciado = enunciado;
		this.idioma = idioma;
		this.categoria = categoria;
		this.imagem = imagem;
		this.estado = estado;
		this.alternativas = alternativas;
		this.grades = grades;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public Idiomas getIdioma() {
		return idioma;
	}

	public void setIdioma(Idiomas idioma) {
		this.idioma = idioma;
	}

	public Categorias getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public List<Alternativa> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(List<Alternativa> alternativas) {
		this.alternativas = alternativas;
	}

	public List<Grade> getGrades() {
		return grades;
	}

	public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}

	@Override
	public String toString() {
		return "Pergunta [enunciado=" + enunciado + ", idioma=" + idioma + ", categoria=" + categoria + ", imagem="
				+ imagem + ", estado=" + estado + ", alternativas=" + alternativas + ", grades=" + grades + ", id=" + id
				+ ", isNew()=" + isNew() + "]";
	}
}

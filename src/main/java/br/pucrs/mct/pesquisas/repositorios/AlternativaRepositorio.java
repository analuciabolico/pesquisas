package br.pucrs.mct.pesquisas.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.pucrs.mct.pesquisas.modelos.Alternativa;

@Repository
public interface AlternativaRepositorio extends JpaRepository<Alternativa, Long>{
	
}

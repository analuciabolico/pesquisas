package br.pucrs.mct.pesquisas.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.pucrs.mct.pesquisas.modelos.Secao;

@Repository
public interface SecaoRepositorio extends JpaRepository<Secao, Long> {

}

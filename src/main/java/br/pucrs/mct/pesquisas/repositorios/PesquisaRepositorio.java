package br.pucrs.mct.pesquisas.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.modelos.Pesquisa;

@Repository
public interface PesquisaRepositorio extends JpaRepository<Pesquisa, Long> {

	// Buscar Todas as pesquisas por Estados
	List<Pesquisa> findByEstadoOrderByNomeAsc(Estados estado);

	// Buscar Todas as pesquisas por Nome
	List<Pesquisa> findByNomeIgnoreCaseContainingOrderByNomeAsc(String nome);

	// Buscar Todas as pesquisas por Idioma
	List<Pesquisa> findByIdiomaOrderByNomeAsc(Idiomas idioma);

	// Buscar Todas as pesquisas por Estados com filtro de Idioma
	List<Pesquisa> findByEstadoAndIdiomaOrderByNomeAsc(Estados estado, Idiomas idioma);

	// Buscar Todas as pesquisas por Estados com filtro de nome
	List<Pesquisa> findByEstadoAndNomeIgnoreCaseContainingOrderByNomeAsc(Estados estado, String nome);

	// Buscar Todas as pesquisas por Idioma sem filtros
	List<Pesquisa> findByNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(String nome, Idiomas idioma);

	// Buscar Todas as pesquisas por Estados com filtro de nome e idioma
	List<Pesquisa> findByEstadoAndNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(Estados estado, String nome,
			Idiomas idioma);
	
	List<Pesquisa> findByPerfil(Perfil perfil);
	
}

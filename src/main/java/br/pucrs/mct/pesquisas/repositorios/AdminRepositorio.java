package br.pucrs.mct.pesquisas.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pucrs.mct.pesquisas.modelos.UserSystem;

public interface AdminRepositorio extends JpaRepository<UserSystem, Integer> {
	Optional<UserSystem> findByNome(String nome);

}

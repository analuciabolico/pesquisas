package br.pucrs.mct.pesquisas.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;

public interface PerfilRepositorio extends JpaRepository<Perfil, Long> {

	// Buscar todos em ordem alfabetica
	List<Perfil> findAllByOrderByNomeAsc();

	// Buscar por Nome
	List<Perfil> findByNomeIgnoreCaseContainingOrderByNomeAsc(String nome);

	// Buscar por estado
	List<Perfil> findByEstadoOrderByNomeAsc(Estados estado);

	// Buscar por idioma
	List<Perfil> findByIdiomaOrderByNomeAsc(Idiomas idioma);

	// Buscar por nome e estado
	List<Perfil> findByEstadoAndNomeIgnoreCaseContainingOrderByNomeAsc(Estados estado, String nome);

	// Buscar estados e idioma
	List<Perfil> findByEstadoAndIdiomaOrderByNomeAsc(Estados estado, Idiomas idioma);

	// Buscar por nome e idioma
	List<Perfil> findByNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(String nome, Idiomas idioma);

	// Buscar por estado, nome e idioma
	List<Perfil> findByEstadoAndNomeIgnoreCaseContainingAndIdiomaOrderByNomeAsc(Estados estado, String nome,
			Idiomas idioma);

	boolean findByNome(String nome);

}

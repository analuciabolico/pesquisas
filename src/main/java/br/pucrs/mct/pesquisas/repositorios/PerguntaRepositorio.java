package br.pucrs.mct.pesquisas.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.pucrs.mct.pesquisas.modelos.Categorias;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Pergunta;

@Repository
public interface PerguntaRepositorio extends JpaRepository<Pergunta, Long> {

	// Buscar Todas as perguntas com filtro de enunciado
	List<Pergunta> findByEnunciadoIgnoreCaseContainingOrderByEnunciadoAsc(String enunciado);

	// Buscar Todas as perguntas ativas com filtro de idioma e categoria
	List<Pergunta> findByIdiomaAndCategoriaOrderByEnunciadoAsc(Idiomas idioma, Categorias categoria);

	// Buscar Todas as perguntas com filtro de idioma e categoria e estado
	List<Pergunta> findByEstadoAndIdiomaAndCategoriaOrderByEnunciadoAsc(Estados estado, Idiomas idioma,
			Categorias categoria);

	// Buscar Todas as perguntas com filtro de Idioma
	List<Pergunta> findByIdiomaOrderByEnunciadoAsc(Idiomas idioma);

	// Buscar Todas as perguntas com filtro de enunciado e categoria
	List<Pergunta> findByEnunciadoIgnoreCaseContainingAndCategoriaOrderByEnunciadoAsc(String enunciado,
			Categorias categoria);

	// Buscar Todas as perguntas com filtro de enunciado e categoria e estado
	List<Pergunta> findByEstadoAndEnunciadoIgnoreCaseContainingAndCategoriaOrderByEnunciadoAsc(Estados estado,
			String enunciado, Categorias categoria);

	// Buscar Todas as perguntas com filtro de categoria
	List<Pergunta> findByCategoriaOrderByEnunciadoAsc(Categorias categoria);

	// Buscar Todas as perguntas com filtro de enunciado e idioma
	List<Pergunta> findByEnunciadoIgnoreCaseContainingAndIdiomaOrderByEnunciadoAsc(String enunciado, Idiomas idioma);

	// Buscar Todas as perguntas com filtro de enunciado e idioma e estado
	List<Pergunta> findByEstadoAndEnunciadoIgnoreCaseContainingAndIdiomaOrderByEnunciadoAsc(Estados estado,
			String enunciado, Idiomas idioma);

	// Buscar Todas as perguntas com filtro de Estado
	List<Pergunta> findByEstadoOrderByEnunciadoAsc(Estados estado);

	// Buscar Todas as perguntas ativas com filtro de enunciado, idioma e categoria
	List<Pergunta> findByEnunciadoIgnoreCaseContainingAndIdiomaAndCategoriaOrderByEnunciadoAsc(String enunciado,
			Idiomas idioma, Categorias categoria);

	List<Pergunta> findAllByOrderByEnunciadoAsc();

	List<Pergunta> findByEnunciadoIgnoreCaseContainingAndIdiomaAndCategoriaAndEstadoOrderByEnunciadoAsc(
			String enunciado, Idiomas idioma, Categorias categoria, Estados estado);

	List<Pergunta> findByEnunciadoIgnoreCaseContainingAndEstadoOrderByEnunciadoAsc(String enunciado, Estados estado);

	List<Pergunta> findByEstadoAndIdiomaOrderByEnunciadoAsc(Estados estado, Idiomas idioma);

	List<Pergunta> findByEstadoAndCategoriaOrderByEnunciadoAsc(Estados estado, Categorias categoria);

	List<Pergunta> findByEstadoAndEnunciadoIgnoreCaseContainingAndIdiomaAndCategoriaOrderByEnunciadoAsc(Estados estado,
			String enunciado, Idiomas idioma, Categorias categoria);

	List<Pergunta> findByEstadoAndIdiomaAndEnunciadoIgnoreCaseContainingOrderByEnunciadoAsc(Estados estado,
			Idiomas idioma, String enunciado);

	@Override
	// Desativa a pergunta (Exclusao logica)
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE Perguntas p SET ativa = false WHERE p.id =:id", nativeQuery = true)
	void deleteById(@Param("id") Long id);

}

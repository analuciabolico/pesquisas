package br.pucrs.mct.pesquisas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PesquisasApplication {

	public static void main(final String[] args) {
		SpringApplication.run(PesquisasApplication.class, args);
	}

	
}

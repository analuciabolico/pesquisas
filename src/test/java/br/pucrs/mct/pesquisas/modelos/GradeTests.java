package br.pucrs.mct.pesquisas.modelos;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.pucrs.mct.pesquisas.PesquisasApplicationTest;

class GradeTests extends PesquisasApplicationTest {

    private static final Idiomas TEST_PERGUNTA_IDIOMA = Idiomas.PORTUGUES;
    private static final Estados TEST_PERGUNTA_ESTADOS = Estados.ATIVADA;

    Grade exampleA;
    Grade exampleB;
    Grade exampleC;
    Pergunta pergunta = new Pergunta();
    List<Grade> grade = new ArrayList<>();

    @BeforeEach
    public void setup() {
        //contrutor Grade(enunciado, perguntas)
        exampleA = new Grade();
        exampleB = new Grade("Grade 1", null);
        exampleC = new Grade("Grade 2", pergunta);
        grade.add(exampleA);
        grade.add(exampleB);
        grade.add(exampleC);
        pergunta.setEnunciado("Pergunta 1");
        pergunta.setIdioma(TEST_PERGUNTA_IDIOMA);
        pergunta.setCategoria(Categorias.DISSERTATIVA);
        pergunta.setImagem("img");
        pergunta.setEstado(TEST_PERGUNTA_ESTADOS);
        pergunta.setAlternativas(null);
        pergunta.setGrades(grade);
    }

    @Test
    void perguntaInitTest() {
        Assertions.assertTrue(true);
        Assertions.assertNotNull(exampleA);
        Assertions.assertNotNull(exampleB);
        Assertions.assertNotNull(exampleC);
        Assertions.assertNotNull(pergunta);
        Assertions.assertNotNull(grade);
    }

    @Test
    void gradeATest() {
        Assertions.assertNull(exampleA.getEnunciado());
        Assertions.assertNull(exampleA.getPergunta());
    }

    @Test
    void gradeBTest() {
        Assertions.assertEquals("Grade 1", exampleB.getEnunciado());
        Assertions.assertNull(exampleB.getPergunta());
    }

    @Test
    void gradeCTest() {
        Assertions.assertEquals("Grade 2", exampleC.getEnunciado());
        Assertions.assertEquals(pergunta, exampleC.getPergunta());
    }
}

package br.pucrs.mct.pesquisas.modelos;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.pucrs.mct.pesquisas.PesquisasApplicationTest;

/**
 * ____________________________________________
 * <p>
 * For the ATRIBUTOS Boolean: - ativa: - true = ATIVA - false = DESTIVADA Attributes:
 *
 * @NotEmpty String enunciado;
 * @NotNull Boolean idioma = false; public enum categorias {DISSERTATIVA, CHECKBOX, MULTIPLA_ESCOLHA, GRADE;}
 * @NotNull categorias tipoObjetiva; String imagem;
 * @NotNull Boolean ativa = true;
 * @OneToMany List<Alternativa> alternativas;
 * @OneToMany List<Grade> grades; ____________________________________________
 */
class PerguntaTests extends PesquisasApplicationTest {

    private static final Idiomas TEST_PERGUNTA_IDIOMA = Idiomas.PORTUGUES;
    private static final Estados TEST_PERGUNTA_ESTADO = Estados.ATIVADA;

    Pergunta perguntaA, perguntaB, perguntaC, perguntaD;
    Alternativa alternativaA, alternativaB, alternativaC, alternativaD;
    Grade gradeA, gradeB, gradeC;
    List<Alternativa> alternativasA = new ArrayList<>();
    List<Alternativa> alternativasB = new ArrayList<>();
    List<Grade> gradesA = new ArrayList<>();

    @BeforeEach
    public void setup() {
        //contrutor Pergunta(enunciado,idioma,tipoObjetiva,imagem,ativa,alternativas,grades)
        perguntaA = new Pergunta();
        perguntaB = new Pergunta(
                "Pergunta 1",
                TEST_PERGUNTA_IDIOMA,
                Categorias.DISSERTATIVA,
                "img",
                TEST_PERGUNTA_ESTADO,
                null,
                null);
        perguntaC = new Pergunta(
                "Pergunta 2",
                TEST_PERGUNTA_IDIOMA,
                Categorias.DISSERTATIVA,
                "img",
                TEST_PERGUNTA_ESTADO,
                alternativasA,
                null);
        perguntaD = new Pergunta(
                "Pergunta 3",
                TEST_PERGUNTA_IDIOMA,
                Categorias.DISSERTATIVA,
                "img",
                TEST_PERGUNTA_ESTADO,
                alternativasB,
                gradesA);
        alternativaA = new Alternativa("alternativa 1", true, perguntaC);
        alternativaB = new Alternativa("alternativa 2", true, perguntaC);
        alternativaC = new Alternativa("alternativa 3", true, perguntaD);
        alternativaD = new Alternativa("alternativa 4", true, perguntaD);
        gradeA = new Grade();
        gradeB = new Grade();
        gradeC = new Grade();
        alternativasA.add(alternativaA);
        alternativasA.add(alternativaB);
        alternativasB.add(alternativaC);
        alternativasB.add(alternativaD);
        gradesA.add(gradeA);
        gradesA.add(gradeB);
        gradesA.add(gradeC);
    }

    @Test
    void perguntaInitTest() {
        Assertions.assertTrue(true);
        Assertions.assertNotNull(perguntaA);
        Assertions.assertNotNull(perguntaB);
        Assertions.assertNotNull(perguntaC);
        Assertions.assertNotNull(alternativaA);
        Assertions.assertNotNull(alternativaB);
        Assertions.assertNotNull(alternativaC);
        Assertions.assertNotNull(gradeA);
        Assertions.assertNotNull(gradeB);
        Assertions.assertNotNull(gradeC);
        Assertions.assertNotNull(alternativasA);
        Assertions.assertNotNull(gradesA);
    }

    @Test
    void perguntaATest() {
        Assertions.assertNull(perguntaA.getEnunciado());
        Assertions.assertNull(perguntaA.getIdioma());
        Assertions.assertNull(perguntaA.getCategoria());
        Assertions.assertNull(perguntaA.getImagem());
        Assertions.assertNull(perguntaA.getEstado());
        Assertions.assertNull(perguntaA.getAlternativas());
        Assertions.assertNull(perguntaA.getGrades());
    }

    @Test
    void perguntaBTest() {
        Assertions.assertEquals("Pergunta 1", perguntaB.getEnunciado());
        Assertions.assertEquals(TEST_PERGUNTA_IDIOMA, perguntaB.getIdioma());
        Assertions.assertEquals(Categorias.DISSERTATIVA, perguntaB.getCategoria());
        Assertions.assertEquals("img", perguntaB.getImagem());
        Assertions.assertEquals(TEST_PERGUNTA_ESTADO, perguntaB.getEstado());
        Assertions.assertNull(perguntaB.getAlternativas());
        Assertions.assertNull(perguntaB.getGrades());
    }

    @Test
    void perguntaCTest() {
        Assertions.assertEquals("Pergunta 2", perguntaC.getEnunciado());
        Assertions.assertEquals(TEST_PERGUNTA_IDIOMA, perguntaC.getIdioma());
        Assertions.assertEquals(Categorias.DISSERTATIVA, perguntaC.getCategoria());
        Assertions.assertEquals("img", perguntaC.getImagem());
        Assertions.assertEquals(TEST_PERGUNTA_ESTADO, perguntaC.getEstado());
        Assertions.assertEquals(alternativasA, perguntaC.getAlternativas());
        Assertions.assertNull(perguntaC.getGrades());
    }

    @Test
    void perguntaDTest() {
        Assertions.assertEquals("Pergunta 3", perguntaD.getEnunciado());
        Assertions.assertEquals(TEST_PERGUNTA_IDIOMA, perguntaD.getIdioma());
        Assertions.assertEquals(Categorias.DISSERTATIVA, perguntaD.getCategoria());
        Assertions.assertEquals("img", perguntaD.getImagem());
        Assertions.assertEquals(TEST_PERGUNTA_ESTADO, perguntaD.getEstado());
        Assertions.assertEquals(alternativasB, perguntaD.getAlternativas());
        Assertions.assertEquals(gradesA, perguntaD.getGrades());
    }

}

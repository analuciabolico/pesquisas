package br.pucrs.mct.pesquisas.modelos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.pucrs.mct.pesquisas.PesquisasApplicationTest;

/*
		assertEquals():		Testa igualdade entre dois objetos (esperado x retornado).
		assertFalse(): 		Testa Retorno booleano FALSO.
		assertTrue(): 		Testa Retorno booleano VERDADEIRO.
		assertNotNull(): 	Testa se um valor de um objeto NÃO está NULO.
		assertNull(): 		Testa se um valor de um objeto está NULO.
*/


/**
 * ________________________________________ For the ATRIBUTOS Boolean: - idiomas: - true = INGLES(EN) - false =
 * PORTUGUES(PT)
 * <p>
 * Attributes:
 *
 * @NotEmpty String legenda;
 * @NotNull Boolean idioma;
 * @ManyToOne Pergunta perguntas; ____________________________________________
 */

@DisplayName("Testes na classe Alternativas")
class AlternativaTests extends PesquisasApplicationTest {
    Alternativa exampleA;
    Alternativa exampleB;
    Alternativa exampleC;
    Pergunta pergunta;

    @BeforeEach
    public void setup() {
        Assertions.assertNull(pergunta);
        Assertions.assertNull(exampleA);
        Assertions.assertNull(exampleB);
        Assertions.assertNull(exampleC);
        pergunta = new Pergunta();
        exampleA = new Alternativa();
        exampleB = new Alternativa("alternativa 2", true, null);
        exampleC = new Alternativa("alternativa 3", true, pergunta);
    }

    @Test
    void alternativaInitTest() {
        Assertions.assertNotNull(pergunta);
        Assertions.assertNotNull(exampleA);
        Assertions.assertNotNull(exampleB);
        Assertions.assertNotNull(exampleC);
    }

    @Test
    void exampleATest() {
        Assertions.assertNull(exampleA.getLegenda());
        Assertions.assertNull(exampleA.getIdioma());
        Assertions.assertNull(exampleA.getPergunta());
        Assertions.assertNull(exampleA.getLegenda());
        Assertions.assertNull(exampleA.getIdioma());
        Assertions.assertNull(exampleA.getPergunta());
    }

    @Test
    void exampleBTest() {
        Assertions.assertNull(exampleB.getPergunta());
        Assertions.assertTrue(exampleB.getIdioma());
        Assertions.assertEquals("alternativa 2", exampleB.getLegenda());
        Assertions.assertEquals(true, exampleB.getIdioma());
        Assertions.assertNull(exampleB.getPergunta());
    }

    @Test
    void exampleCTest() {
        Assertions.assertTrue(exampleC.getIdioma());
        Assertions.assertEquals("alternativa 3", exampleC.getLegenda());
        Assertions.assertEquals(true, exampleC.getIdioma());
        Assertions.assertEquals(pergunta, exampleC.getPergunta());
    }

}

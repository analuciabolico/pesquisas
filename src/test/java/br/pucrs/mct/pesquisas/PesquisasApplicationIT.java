package br.pucrs.mct.pesquisas;


import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@Tag("integration")
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WithMockUser(roles = "admin")
public class PesquisasApplicationIT {


}

package br.pucrs.mct.pesquisas.servicos;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.pucrs.mct.pesquisas.PesquisasApplicationIT;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Pergunta;
import br.pucrs.mct.pesquisas.repositorios.PerguntaRepositorio;

import static br.pucrs.mct.pesquisas.modelos.Categorias.DISSERTATIVA;

class PerguntaServicoTests extends PesquisasApplicationIT {

    private static final Long TEST_PERGUNTA_ID = 1L;
    private static final Long TEST_PERGUNTA_ID2 = 2L;
    private static final Long TEST_PERGUNTA_ID3 = 3L;
    private static final Long TEST_PERGUNTA_ID4 = 4L;
    private static final Estados TEST_PERGUNTA_ESTADO_ATIVADO = Estados.ATIVADA;
    private static final Estados TEST_PERGUNTA_ESTADO_DESATIVADO = Estados.DESATIVADA;
    private static final String ENUNCIADO = "Qual sua idade?";
    private static final String ENUNCIADO2 = "O que você mais gostou?";
    private static final String ENUNCIADO3 = "Qual o seu nome?";
    private static final String ENUNCIADO4 = "What is your name?";
    private static final Idiomas TEST_PERGUNTA_IDIOMA1 = Idiomas.PORTUGUES;
    private static final Idiomas TEST_PERGUNTA_IDIOMA2 = Idiomas.INGLES;

    @Autowired
    private PerguntaRepositorio perguntaRepositorio;

    @Autowired
    private PerguntaServico perguntaServico;

    private Pergunta pergunta;
    private Pergunta pergunta2;
    private Pergunta pergunta3;
    private Pergunta pergunta4;

    @BeforeEach
    public void setup() {
        pergunta = new Pergunta();
        pergunta.setId(TEST_PERGUNTA_ID);
        pergunta.setEnunciado(ENUNCIADO);
        pergunta.setIdioma(TEST_PERGUNTA_IDIOMA1);
        pergunta.setCategoria(DISSERTATIVA);
        pergunta.setImagem("img");
        pergunta.setEstado(TEST_PERGUNTA_ESTADO_ATIVADO);
        pergunta.setAlternativas(null);
        pergunta.setGrades(null);

        pergunta2 = new Pergunta();
        pergunta2.setId(TEST_PERGUNTA_ID2);
        pergunta2.setEnunciado(ENUNCIADO2);
        pergunta2.setIdioma(TEST_PERGUNTA_IDIOMA1);
        pergunta2.setCategoria(DISSERTATIVA);
        pergunta2.setImagem("img");
        pergunta2.setEstado(TEST_PERGUNTA_ESTADO_ATIVADO);
        pergunta2.setAlternativas(null);
        pergunta2.setGrades(null);

        pergunta3 = new Pergunta();
        pergunta3.setId(TEST_PERGUNTA_ID3);
        pergunta3.setEnunciado(ENUNCIADO3);
        pergunta3.setIdioma(TEST_PERGUNTA_IDIOMA1);
        pergunta3.setCategoria(DISSERTATIVA);
        pergunta3.setImagem("img");
        pergunta3.setEstado(TEST_PERGUNTA_ESTADO_DESATIVADO);
        pergunta3.setAlternativas(null);
        pergunta3.setGrades(null);

        pergunta4 = new Pergunta();
        pergunta4.setId(TEST_PERGUNTA_ID4);
        pergunta4.setEnunciado(ENUNCIADO4);
        pergunta4.setIdioma(TEST_PERGUNTA_IDIOMA2);
        pergunta4.setCategoria(DISSERTATIVA);
        pergunta4.setImagem("img");
        pergunta4.setEstado(TEST_PERGUNTA_ESTADO_ATIVADO);
        pergunta4.setAlternativas(null);
        pergunta4.setGrades(null);
    }

    @Test
    void salvarTest() {
        perguntaServico.salvar(pergunta);
        Pergunta perguntaSalva = perguntaServico.buscarPorId(TEST_PERGUNTA_ID);
        Assertions.assertNotNull(perguntaSalva);
    }

    @Test
    void buscarPorIdTest() throws Exception {
        perguntaServico.salvar(pergunta);
        Pergunta perguntaSalva = perguntaServico.buscarPorId(TEST_PERGUNTA_ID);
        Assertions.assertEquals(TEST_PERGUNTA_ID, perguntaSalva.getId());
        Assertions.assertEquals(ENUNCIADO, perguntaSalva.getEnunciado());
        Assertions.assertEquals(TEST_PERGUNTA_IDIOMA1, perguntaSalva.getIdioma());
        Assertions.assertEquals(DISSERTATIVA, perguntaSalva.getCategoria());
        Assertions.assertEquals("img", perguntaSalva.getImagem());
        Assertions.assertEquals(TEST_PERGUNTA_ESTADO_ATIVADO, perguntaSalva.getEstado());
    }

    @Test
    void listarTodasPerguntasTest() throws Exception {
        perguntaRepositorio.saveAll(Arrays.asList(pergunta2, pergunta3, pergunta4));
        List<Pergunta> listaTeste = perguntaServico.buscarTodasPerguntas();
        Assertions.assertEquals(3, listaTeste.size());
    }

}

package br.pucrs.mct.pesquisas.controladores;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.pucrs.mct.pesquisas.PesquisasApplicationIT;
import br.pucrs.mct.pesquisas.modelos.Alternativa;
import br.pucrs.mct.pesquisas.modelos.Categorias;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Grade;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Pergunta;
import br.pucrs.mct.pesquisas.repositorios.PerguntaRepositorio;
import br.pucrs.mct.pesquisas.servicos.PerguntaServico;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


class PerguntaControladorTests extends PesquisasApplicationIT {

    private static final Long TEST_PERGUNTA_ID = 2L;
    private static final Long TEST_PERGUNTA_ID_EDITAR = 1L;
    private static final String TEST_VIEW_PERGUNTA_NEW = "perguntas/novaPergunta";
    private static final String TEST_PERGUNTA_NOVA = "/perguntas/nova";
    private static final String PERGUNTA_REDIRECT = "redirect:/perguntas";
    private static final Idiomas TEST_PERGUNTA_IDIOMA = Idiomas.PORTUGUES;
    private static final Categorias TEST_PERGUNTA_CATEGORIA = Categorias.DISSERTATIVA;
    private static final Estados TEST_PERGUNTA_ESTADO = Estados.ATIVADA;
    private static final String PERGUNTA_EXCLUIR = "/perguntas/{id}/excluir";
    private static final String PERGUNTA_LISTAR = "perguntas/listarPergunta";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private PerguntaRepositorio perguntaRepositorio;

    @MockBean
    private PerguntaServico perguntaServico;

    private Pergunta pergunta;
    private Pergunta perguntaEditar;
    private List<Alternativa> alternativas = new ArrayList<>();
    private List<Grade> grades = new ArrayList<>();
    private Alternativa alternativa;
    private Grade grade;

    @BeforeEach
    public void configuracao() {
        pergunta = new Pergunta();// ("Qual é o seu nome",true,"DISSERTATIVA","img",true,alternativas,grades);
        pergunta.setEnunciado("Qual sua idade");
        pergunta.setEstado(TEST_PERGUNTA_ESTADO);
        pergunta.setIdioma(TEST_PERGUNTA_IDIOMA);
        pergunta.setId(TEST_PERGUNTA_ID);
        pergunta.setCategoria(Categorias.DISSERTATIVA);
        alternativa = new Alternativa("Nome", false, pergunta);
        grade = new Grade("Qual é seu nome", pergunta);
        alternativas.add(alternativa);
        grades.add(grade);
        pergunta.setAlternativas(alternativas);
        pergunta.setGrades(grades);

        perguntaEditar = new Pergunta();
        perguntaEditar.setId(TEST_PERGUNTA_ID_EDITAR);
        perguntaEditar.setEnunciado("Qual sua idade?");
        perguntaEditar.setIdioma(TEST_PERGUNTA_IDIOMA);
        perguntaEditar.setCategoria(TEST_PERGUNTA_CATEGORIA);
        given(this.perguntaServico.buscarPorId(TEST_PERGUNTA_ID_EDITAR)).willReturn(perguntaEditar);
    }

    @Test
    void mostrarTodasTest() throws Exception {
        given(this.perguntaServico.buscarTodasPerguntas()).willReturn(Lists.newArrayList());
        mockMvc.perform(MockMvcRequestBuilders.get("/perguntas"))
               .andExpect(MockMvcResultMatchers.status()
                                               .isOk())
               .andDo(MockMvcResultHandlers.print())
               .andExpect(MockMvcResultMatchers.model()
                                               .attributeExists("perguntas"));
    }

    @Test
    void buscarTodosOsFiltroTest() throws Exception {
        mockMvc.perform(post("/perguntas/").param("enunciado", pergunta.getEnunciado())
                                           .param("idioma", "PORTUGUES")
                                           .param("categoria", "DISSERTATIVA")
                                           .param("estado", "ATIVADA"))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name("perguntas/listarPergunta"));
    }

    @Test
    void buscarPorEnunciadoTest() throws Exception {
        mockMvc.perform(post("/perguntas/").param("enunciado", "Qual sua idade?")
                                           .param("idioma", "")
                                           .param("categoria", "")
                                           .param("estado", ""))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name(PERGUNTA_LISTAR));
    }

    @Test
    void buscarPorIdiomaTest() throws Exception {
        mockMvc.perform(post("/perguntas/").param("enunciado", "")
                                           .param("idioma", "PORTUGUES")
                                           .param("categoria", "")
                                           .param("estado", ""))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name("perguntas/listarPergunta"));
    }

    @Test
    void buscarPorIdiomaECategoriaTest() throws Exception {
        mockMvc.perform(post("/perguntas/").param("enunciado", "")
                                           .param("idioma", "PORTUGUES")
                                           .param("categoria", "DISSERTATIVA")
                                           .param("estado", ""))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name("perguntas/listarPergunta"));
    }

    @Test
    void buscarPesquisarPorIdiomaEEnunciadoTest() throws Exception {
        mockMvc.perform(post("/perguntas/").param("enunciado", pergunta.getEnunciado())
                                           .param("idioma", "PORTUGUES")
                                           .param("categoria", "")
                                           .param("estado", ""))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name("perguntas/listarPergunta"));
    }

    @Test
    void buscarPorCategoriaEEnunciadoTest() throws Exception {
        mockMvc.perform(post("/perguntas/").param("enunciado", pergunta.getEnunciado())
                                           .param("idioma", "")
                                           .param("categoria", "DISSERTATIVA")
                                           .param("estado", ""))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name("perguntas/listarPergunta"));
    }

    @Test
    void novaPerguntaTest() throws Exception {
        mockMvc.perform(get(TEST_PERGUNTA_NOVA))
               .andExpect(status().isOk())
               .andExpect(view().name(TEST_VIEW_PERGUNTA_NEW));
    }

    @Test
    void novaPerguntaPostTest() throws Exception {
        mockMvc.perform(post(TEST_PERGUNTA_NOVA).param("enunciado", "Adulto")
                                                .param("idioma", "PORTUGUES")
                                                .param("categoria", "DISSERTATIVA")
                                                .param("estado", ""))
               .andExpect(status().isOk())
               .andExpect(view().name("perguntas/novaPergunta"));
    }

    @Test
    void novaPerguntaPostErrosTest() throws Exception {
        mockMvc.perform(post(TEST_PERGUNTA_NOVA).param("enunciado", "Adulto")
                                                .param("idioma", "PORTUGUES")
                                                .param("categoria", "")
                                                .param("estado", ""))
               .andExpect(status().isOk())
               .andExpect(view().name("perguntas/novaPergunta"));
    }

    @Test
    void excluirPerguntaTest() throws Exception {
        mockMvc.perform(post(PERGUNTA_EXCLUIR, TEST_PERGUNTA_ID).param("id", "2"))
               .andExpect(status().is3xxRedirection())
               .andExpect(view().name(PERGUNTA_REDIRECT));
    }

    @Test
    void limparTodosFiltrosTest() throws Exception {
        mockMvc.perform(get("/perguntas"))
               .andExpect(status().isOk())
               .andExpect(view().name("perguntas/listarPergunta"));
    }
}

package br.pucrs.mct.pesquisas.controladores;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.PesquisasApplicationIT;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.servicos.PerfilServico;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@AutoConfigureMockMvc
class PerfilControladorTest extends PesquisasApplicationIT {

    private static final Long TESTE_ID_PERFIL = 1L;
    private static final String TESTE_VISUALIZAR_NOVO_PERFIL = "perfis/novoPerfil";
    private static final String TESTE_NOVO_PERFIL = "/perfis/novo";
    private static final String TESTE_PERFIL = "/perfis";
    private static final String TESTE_LISTA_PERFIL = "perfis/listarPerfil";

    private static final Idiomas TEST_PERFIL_IDIOMA = Idiomas.PORTUGUES;

    private static final String EDITAR_PERFIL = "perfis/editarPerfil";
    private static final String REDIRECIONAR_EDITAR_PERFIL = "redirect:/perfis/{id}/editar";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PerfilServico perfis;

    private Perfil crianca;
    private Perfil adulto;

    @BeforeEach
    public void setup() {

        crianca = new Perfil();
        crianca.setId(TESTE_ID_PERFIL);
        crianca.setNome("Crianças");
        crianca.setIdioma(TEST_PERFIL_IDIOMA);
        crianca.setMinIdade(5);
        crianca.setMaxIdade(9);
        crianca.setEstado(Estados.ATIVADA);

        given(this.perfis.buscarPorId(TESTE_ID_PERFIL)).willReturn(crianca);

        // ------------------------//
        adulto = new Perfil();
        adulto.setId(2L);
        adulto.setNome("Adulto");
        adulto.setIdioma(TEST_PERFIL_IDIOMA);
        adulto.setMinIdade(5);
        adulto.setMaxIdade(9);
        adulto.setEstado(Estados.ATIVADA);

    }

    @Test
    @DisplayName("Teste para validar a view de cricao de novo perfil retorna estatus 200")
    void testeCaminhoNovoPerfil() throws Exception {
        mockMvc.perform(get(TESTE_NOVO_PERFIL))
               .andExpect(status().isOk())
               .andExpect(view().name(TESTE_VISUALIZAR_NOVO_PERFIL));
    }

    @Test
    @DisplayName("Teste para validar se enviando um perfil valido ele retorna status 200")
    void testeCriacaoPerfilSucesso() throws Exception {
        mockMvc.perform(post(TESTE_NOVO_PERFIL).param("nome", "Adulto")
                                               .param("min_idade", "20")
                                               .param("max_idade", "30")
                                               .param("idioma", TEST_PERFIL_IDIOMA.toString()))
               .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Teste para validar se a lista de perfis retorna a view name com nome '/perfis' e se o status é 200")
    void testeListaEstado() throws Exception {
        given(this.perfis.buscarPorEstado(crianca.getEstado())).willReturn(Lists.newArrayList(crianca));
        mockMvc.perform(get(TESTE_PERFIL))
               .andExpect(status().isOk())
               .andExpect(view().name(TESTE_LISTA_PERFIL));

    }

    @Test
    @DisplayName("Teste para validar perfis retorna a view name com nome '/perfis' e se o status é 200 com filtro de nome e idioma")
    void testeFiltroPerfilNomeAndIdioma() throws Exception {
        given(this.perfis.buscarPorNomeEIdioma("crianca", TEST_PERFIL_IDIOMA))
                .willReturn(Lists.newArrayList(crianca, new Perfil()));
        mockMvc.perform(get(TESTE_PERFIL))
               .andExpect(status().isOk())
               .andExpect(view().name(TESTE_LISTA_PERFIL));
    }

    @Test
    @DisplayName("Teste para validar o filtro por idioma no endpoint /perfis esta retornando a view certa")
    void testeFiltroPerfilIdioma() throws Exception {
        mockMvc.perform(get("/perfis/").param("nome", "Teste")
                                       .param("minIdade", "2")
                                       .param("maxIdade", "5")
                                       .param("idioma", "PORTUGUES")
                                       .param("estado", "ATIVADA"))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name(TESTE_LISTA_PERFIL));

    }

    @Test
    @DisplayName("Teste para validar ")
    void testeFiltroPerfilNome() throws Exception {
        given(this.perfis.buscarPorNome("crianca")).willReturn(Lists.newArrayList(crianca, new Perfil()));
        mockMvc.perform(get(TESTE_PERFIL))
               .andExpect(status().isOk())
               .andExpect(view().name(TESTE_LISTA_PERFIL));
    }

    @Test
    @DisplayName("Teste para validar se a edicao de um perfil tras os dados corretos")
    void testeEditarInitPerfil() throws Exception {

        mockMvc.perform(get("/perfis/{id}/editar", 1L))
               .andExpect(status().isOk())
               .andExpect(model().attributeExists("perfil"))
               .andExpect(model().attribute("perfil", hasProperty("nome", is("Crianças"))))
               .andExpect(model().attribute("perfil", hasProperty("id", is(crianca.getId()))))
               .andExpect(model().attribute("perfil", hasProperty("minIdade", is(5))))
               .andExpect(model().attribute("perfil", hasProperty("maxIdade", is(9))))
               .andExpect(model().attribute("perfil", hasProperty("idioma", is(TEST_PERFIL_IDIOMA))))
               .andExpect(view().name(EDITAR_PERFIL));
    }

    @Test
    @DisplayName("Teste para validar se depois de um salvamento de um perfil voce é redirecionado")
    @Disabled
    void testeEditarPerfil() throws Exception {
        mockMvc.perform(post("/perfis/{id}/editar", TESTE_ID_PERFIL).param("nome", crianca.getNome())
                                                                    .param("minIdade", "5")
                                                                    .param("maxIdade", "9")
                                                                    .param("idioma", "PORTUGUES"))
               .andExpect(status().is3xxRedirection())
               .andExpect(view().name(REDIRECIONAR_EDITAR_PERFIL));

    }

    @Test
    @DisplayName("Teste para validar se filtrando por nome e estado ele tras a view certa")
    void testeNomeEEstadoPerfil() throws Exception {

        mockMvc.perform(get("/perfis/", TESTE_ID_PERFIL).param("nome", crianca.getNome())
                                                        .param("minIdade", "3")
                                                        .param("maxIdade", "5")
                                                        .param("idioma", "PORTUGUES")
                                                        .param("estado", "ATIVADA"))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name(TESTE_LISTA_PERFIL));
    }

    @Test
    @DisplayName("Teste para validar se filtrando por idioma e estado ele tras a view certa")
    void testeIdiomaEEstadoPerfil() throws Exception {

        mockMvc.perform(get("/perfis/", TESTE_ID_PERFIL).param("nome", "Teste")
                                                        .param("minIdade", "3")
                                                        .param("maxIdade", "5")
                                                        .param("idioma", "PORTUGUES")
                                                        .param("estado", "ATIVADA"))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name(TESTE_LISTA_PERFIL));
    }

    @Test
    @DisplayName("Teste para validar se excluindo um perfil pleo id o status é 200 e a view é listagem")
    void testeExcluir() throws Exception {
        perfis.excluir(crianca.getId(), null);
        given(this.perfis.buscar()).willReturn(Lists.newArrayList());
        mockMvc.perform(get(TESTE_PERFIL))
               .andExpect(status().isOk())
               .andDo(MockMvcResultHandlers.print())
               .andExpect(view().name(TESTE_LISTA_PERFIL));

    }

}

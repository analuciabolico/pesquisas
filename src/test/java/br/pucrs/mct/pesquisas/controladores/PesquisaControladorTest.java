package br.pucrs.mct.pesquisas.controladores;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.pucrs.mct.pesquisas.PesquisasApplicationIT;
import br.pucrs.mct.pesquisas.modelos.Estados;
import br.pucrs.mct.pesquisas.modelos.Idiomas;
import br.pucrs.mct.pesquisas.modelos.Perfil;
import br.pucrs.mct.pesquisas.modelos.Pesquisa;
import br.pucrs.mct.pesquisas.servicos.PerfilServico;
import br.pucrs.mct.pesquisas.servicos.PesquisaServico;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WithMockUser(roles = "admin")
class PesquisaControladorTest extends PesquisasApplicationIT {

	private static final String TEST_VIEW_PESQUISA_NEW = "pesquisas/novaPesquisa";
	private static final String TEST_PESQUISA_NOVA = "/pesquisas/nova";
	private static final String TEST_VIEW_PESQUISA_LISTAR = "pesquisas/listarPesquisa";
	private static final String TEST_PESQUISA = "/pesquisas";
	private static final String TEST_REDIRECIONAR_PESQUISA = "redirect:/pesquisas";
	private static final String TEST_PESQUISA_MUDARESTADO = "/pesquisas/{id}/mudarEstado";
	private static final String PESQUISA = "pesquisas";
	private static final String EDITAR_PESQUISA = "pesquisas/editarPesquisa";
	
	private static final String TESTE_NOME1 = "Criança";
	private static final String TESTE_NOME2 = "Adulto";

	private static final Estados TESTE_PESQUISA_ESTADO_ATIVADO = Estados.ATIVADA;
	private static final Idiomas TESTE_IDIOMA_PORTUGUES = Idiomas.PORTUGUES;

	private static final Long TEST_PESQUISA_1 = 1L;
	private static final Long TEST_PESQUISA_2 = 2L;
	private static final Long TESTE_ID_PERFIL1 = 1L;
	private static final Long TESTE_ID_PERFIL2 = 2L;

	@Autowired
	MockMvc mockMvc;

	@MockBean
	private PesquisaServico pesquisa;
	@MockBean
	private PerfilServico perfis;

	private Pesquisa pesquisaTeste;
	private Pesquisa pesquisaTeste2;

	private Perfil crianca;
	private Perfil adulto;

	@BeforeEach
	public void setup() {

		crianca = new Perfil();
		crianca.setId(TESTE_ID_PERFIL1);
		crianca.setNome(TESTE_NOME1);
		crianca.setIdioma(TESTE_IDIOMA_PORTUGUES);
		crianca.setMinIdade(5);
		crianca.setMaxIdade(9);
		crianca.setEstado(Estados.ATIVADA);

		given(this.perfis.buscarPorId(TESTE_ID_PERFIL1)).willReturn(crianca);

		// ------------------------//
		adulto = new Perfil();
		adulto.setId(TESTE_ID_PERFIL2);
		adulto.setNome(TESTE_NOME2);
		adulto.setIdioma(TESTE_IDIOMA_PORTUGUES);
		adulto.setMinIdade(25);
		adulto.setMaxIdade(50);
		adulto.setEstado(Estados.ATIVADA);

		given(this.perfis.buscarPorId(TESTE_ID_PERFIL2)).willReturn(adulto);

		pesquisaTeste = new Pesquisa();
		pesquisaTeste.setId(TEST_PESQUISA_1);
		pesquisaTeste.setNome(TESTE_NOME1);
		pesquisaTeste.setEstado(TESTE_PESQUISA_ESTADO_ATIVADO);
		pesquisaTeste.setIdioma(TESTE_IDIOMA_PORTUGUES);
		pesquisaTeste.setPerfil(crianca);

		given(this.pesquisa.buscarPorId(TEST_PESQUISA_1)).willReturn(pesquisaTeste);

		pesquisaTeste2 = new Pesquisa();
		pesquisaTeste2.setId(TEST_PESQUISA_2);
		pesquisaTeste2.setNome(TESTE_NOME2);
		pesquisaTeste2.setEstado(TESTE_PESQUISA_ESTADO_ATIVADO);
		pesquisaTeste2.setIdioma(TESTE_IDIOMA_PORTUGUES);
		pesquisaTeste2.setPerfil(adulto);
	}

	@Test
	void listarPesquisasSemFiltroTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(TEST_PESQUISA)).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(view().name(TEST_VIEW_PESQUISA_LISTAR))
				.andExpect(MockMvcResultMatchers.model().attributeExists(PESQUISA));
	}

	@Test
	void listarTodosOsFiltroTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(TEST_PESQUISA).param("nome", pesquisaTeste.getNome())
				.param("idioma", TESTE_IDIOMA_PORTUGUES.toString())
				.param("estado", TESTE_PESQUISA_ESTADO_ATIVADO.toString())).andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(view().name(TEST_VIEW_PESQUISA_LISTAR))
				.andExpect(MockMvcResultMatchers.model().attributeExists(PESQUISA));
	}

	@Test
	void listarPesquisaErrosTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(TEST_PESQUISA).param("nome", pesquisaTeste.getNome())
				.param("PORTUGUES", "idioma").param("00", "estado")).andDo(MockMvcResultHandlers.print())
				.andExpect(status().isBadRequest());

	}

	@Test
	void mudarEstadoPesquisaTest() throws Exception {
		mockMvc.perform(post(TEST_PESQUISA_MUDARESTADO, TEST_PESQUISA_1)
				)
				.andDo(MockMvcResultHandlers.print()).andExpect(status().is3xxRedirection())
				.andExpect(view().name(TEST_REDIRECIONAR_PESQUISA));
	}


	@Test
	void testeExcluir() throws Exception {
		RedirectAttributes atribRedirecionamento = null;
		pesquisa.excluir(TEST_PESQUISA_1, atribRedirecionamento);
		given(this.pesquisa.buscar()).willReturn(Lists.newArrayList());
		mockMvc.perform(get(TEST_PESQUISA)).andExpect(status().isOk()).andDo(MockMvcResultHandlers.print())
				.andExpect(view().name(TEST_VIEW_PESQUISA_LISTAR));

	}

	@Test
	void novaPesquisaTest() throws Exception {
		mockMvc.perform(get(TEST_PESQUISA_NOVA)).andExpect(status().isOk())
				.andExpect(view().name(TEST_VIEW_PESQUISA_NEW));
	}

	@Test
	void novaPesquisaPostTest() throws Exception {
		mockMvc.perform(post(TEST_PESQUISA_NOVA).param("nome", "Pesquisas Adulto").param("idioma", "PORTUGUES")
				.param("perfil", "Adulto").param("estado", "")).andExpect(status().isOk())
				.andExpect(view().name("pesquisas/novaPesquisa"));
	}

	@Test
	void novaPesquisaPostErrosTest() throws Exception {
		mockMvc.perform(post(TEST_PESQUISA_NOVA).param("nome", "").param("idioma", "PORTUGUES").param("perfil", "")
				.param("estado", "")).andExpect(status().isOk()).andExpect(view().name("pesquisas/novaPesquisa"));
	}

	@Test
	void testeEditarInitPesquisa() throws Exception {

		mockMvc.perform(get("/pesquisas/{id}/editar", TEST_PESQUISA_1)).andExpect(status().isOk())
				.andExpect(model().attributeExists("pesquisa"))
				.andExpect(model().attribute("pesquisa", hasProperty("nome", is(pesquisaTeste.getNome()))))
				.andExpect(model().attribute("pesquisa", hasProperty("id", is(pesquisaTeste.getId()))))
				.andExpect(model().attribute("pesquisa", hasProperty("idioma", is(pesquisaTeste.getIdioma()))))
				.andExpect(model().attribute("pesquisa", hasProperty("estado", is(pesquisaTeste.getEstado()))))
				.andExpect(model().attribute("pesquisa", hasProperty("perfil", is(pesquisaTeste.getPerfil()))))
				.andExpect(view().name(EDITAR_PESQUISA));
	}

	@Test
	void testeEditarPesquisa() throws Exception {
		mockMvc.perform(post("/pesquisas/{id}/editar", TEST_PESQUISA_1).param("nome", "Pesquisas Criança").param("idioma", "PORTUGUES")
				.param("perfil", "Adulto").param("estado", "ATIVADA")).andExpect(status().is3xxRedirection());

	}
}
